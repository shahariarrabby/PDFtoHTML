/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pdfreader;

/**
 *
 * @author Zia
 */
import java.io.IOException;

//import org.apache.log4j.Logger;
import PDF.exceptions.COSVisitorException;
import PDF.util.PDFMergerUtility;

public class Merge {
//	private static Logger logger = Logger.getLogger(Merge.class);
	
	public void merge2PDF(String Source, String Insert) {
		PDFMergerUtility ut = new PDFMergerUtility();
		ut.addSource(Source);
		ut.addSource(Insert);
		ut.setDestinationFileName(Source);
//		logger.info("Input File: "+Source);
//		logger.info("Adding page: "+Insert);
//		logger.info("Destination: "+Source);
		
		try {
			//merging the two files
			ut.mergeDocuments();
		} catch (COSVisitorException e) {
		} catch (IOException e) {
		}
	}
        
        public static void main(String[] args)
        {
            Merge m = new Merge();
            m.merge2PDF("G://1.pdf", "G://blankpdf.pdf");
        }
}