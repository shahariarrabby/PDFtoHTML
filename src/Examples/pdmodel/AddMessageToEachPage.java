/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package Examples.pdmodel;

import java.io.IOException;
import java.util.List;

import PDF.exceptions.COSVisitorException;

import PDF.pdmodel.PDDocument;
import PDF.pdmodel.PDPage;

import PDF.pdmodel.common.PDRectangle;
import PDF.pdmodel.edit.PDPageContentStream;

import PDF.pdmodel.font.PDFont;
import PDF.pdmodel.font.PDType1Font;


/**
 * This is an example of how to add a message to every page
 * in a pdf document.
 *
 * @author <a href="mailto:ben@benlitchfield.com">Ben Litchfield</a>
 * @version $Revision: 1.3 $
 */
public class AddMessageToEachPage
{
    /**
     * Constructor.
     */
    public AddMessageToEachPage()
    {
        super();
    }

    /**
     * create the second sample document from the PDF file format specification.
     *
     * @param file The file to write the PDF to.
     * @param message The message to write in the file.
     * @param outfile The resulting PDF.
     *
     * @throws IOException If there is an error writing the data.
     * @throws COSVisitorException If there is an error writing the PDF.
     */
    public void doIt( String file, String message, String  outfile ) throws IOException, COSVisitorException
    {
        // the document
        PDDocument doc = null;
        try
        {
            doc = PDDocument.load( file );

            List allPages = doc.getDocumentCatalog().getAllPages();
            PDFont font = PDType1Font.HELVETICA_BOLD;
            float fontSize = 12.0f;

            for( int i=0; i<allPages.size(); i++ )
            {
                PDPage page = (PDPage)allPages.get( i );
                PDRectangle pageSize = page.findMediaBox();
                float stringWidth = font.getStringWidth( message )*fontSize/1000f;
                // calculate to center of the page
                int rotation = page.findRotation(); 
                boolean rotate = rotation == 90 || rotation == 270;
                float pageWidth = rotate ? pageSize.getHeight() : pageSize.getWidth();
                float pageHeight = rotate ? pageSize.getWidth() : pageSize.getHeight();
                System.out.println("Page Width :"+pageWidth+"; Page Height : "+pageHeight);
//                double centeredXPosition = rotate ? pageHeight/2f : (pageWidth - stringWidth)/2f;
//                double centeredYPosition = rotate ? (pageWidth - stringWidth)/2f : pageHeight/2f;
                double centeredXPosition = rotate ? pageHeight-5 : 20;
                double centeredYPosition = rotate ? (stringWidth) : 0;
                System.out.println("X Pos: "+centeredXPosition+ "; Y Pos :"+ centeredYPosition);
                // append the content to the existing stream
                PDPageContentStream contentStream = new PDPageContentStream(doc, page, true, true,true);
                contentStream.beginText();
                // set font and font size
                contentStream.setFont( font, fontSize );
                // set text color to red
                contentStream.setNonStrokingColor(255, 0, 0);
                if (rotate)
                {
                    // rotate the text according to the page rotation
                    contentStream.setTextRotation(Math.PI/2, centeredXPosition, centeredYPosition);
                }
                else
                {
                    contentStream.setTextTranslation(centeredXPosition, centeredYPosition);
                }
                contentStream.drawString( message );
                contentStream.endText();
                contentStream.close();
            }

            doc.save( outfile );
        }
        finally
        {
            if( doc != null )
            {
                doc.close();
            }
        }
    }

    /**
     * This will create a hello world PDF document.
     * <br />
     * see usage() for commandline
     *
     * @param args Command line arguments.
     */
    public static void main(String[] args)
    {
        AddMessageToEachPage app = new AddMessageToEachPage();
        try
        {
//            if( args.length != 3 )
//            {
//                app.usage();
//            }
//            else
//            {
                app.doIt( "G:/From Fuad sir DOCX to PDF/open.pdf", "My name is Zia.", "G:/From Fuad sir DOCX to PDF/open.pdf" );
//            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * This will print out a message telling how to use this example.
     */
    private void usage()
    {
        System.err.println( "usage: " + this.getClass().getName() + " <input-file> <Message> <output-file>" );
    }
}
