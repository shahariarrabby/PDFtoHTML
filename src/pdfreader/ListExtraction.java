/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pdfreader;

import PDF.pdmodel.font.PDFont;
import PDF.pdmodel.font.PDFontDescriptor;
import PDF.util.TextPosition;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 *
 * @author Zia
 */
public class ListExtraction
{    
    private BufferedWriter htmlFile;
    private int type = 0;
    private float zoom = (float) 1;
    private int pCount =0;

    private int marginTopBackground = 0;
    private boolean lastWasBold = true;
    private boolean lastWasItalic = true;

    private int lastMarginTop = 0;
    private int max_gap = 15;

    float previousAveCharWidth = -1;
    private int resolution = 72; //default resolution

    private boolean needToStartNewSpan = false;
    private StringBuffer currentParagraph = new StringBuffer();
    private int lastMarginLeft = 0;
    private int lastMarginRight = 0;
    private int numberSpace = 0;
    private int sizeAllSpace = 0;
    private boolean addSpace;
    private int startXLine;
    private boolean wasBold = false;
    private boolean wasItalic = false;
    private int lastFontSizePx = 0;
    private String lastFontString = "";
    private boolean paragraphcheck = true;    
    String align = null;
	
    private StringBuffer currentLine = new StringBuffer();
    List<Integer> firstCharOfLineStartsAt = new ArrayList<Integer>();
    List<Integer> lastCharOfLineEndsAt = new ArrayList<Integer>();


   /**
    * Public constructor
    * @param outputFileName The html file
    * @param type represents how we are going to create the html file
    * 			0: we create a new block for every letters
    * 			1: we create a new block for every words
    * 			2: we create a new block for every line 
    * 			3: we create a new block for every line - using a cache to set the word-spacing property
    * @param zoom 1.5 - 2 is a good range
    * @throws IOException
    */
    public ListExtraction(String outputFileName, int type, float zoom) throws IOException
    {
    	try 
        {
            htmlFile = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFileName),"UTF8"));
            String header = "<html>" +
                            "<head>" +
                            "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>" +
                            "<title>Html file</title>" +
                            "<link rel=\"stylesheet\" href=\"css/style.css\" />" +
                            "</head>" +
                            "<body>";//<table border="+25+"% width = "+80+"% align = center bgcolor = pink>";
            htmlFile.write(header);
            this.type = type;
            this.zoom= zoom;		
        }
    	catch (UnsupportedEncodingException e) 
        {
            System.err.println( "Error: Unsupported encoding." );
            System.exit( 1 );		
        }
    	catch (FileNotFoundException e) 
        {
            System.err.println( "Error: File not found." );
            System.exit( 1 );		
        }
    	catch (IOException e) 
        {
            System.err.println( "Error: IO error, could not open html file." );
            System.exit( 1 );	
        }	
    }
	
    
    public ListExtraction(int type, float zoom) throws IOException
    {    	
        this.type = type;
        this.zoom= zoom;
        this.layerCount = 0;
    }
    /**
     * Close the HTML file
     */
    public void closeFile() 
    {		
        try 
        {
            htmlFile.close();

        } 
        catch (IOException e) 
        {
            System.err.println( "Error: IO error, could not close html file." );            
            System.exit( 1 );		
        }
    }
    /**
     * A method provided as an event interface to allow a subclass to perform
     * some specific functionality when text needs to be processed.
     *
     * @param text The text to be processed
     */
    public void processTextt( TextPosition text )
    {
    	try 
        {
            int marginLeft = (int)((text.getXDirAdj())*zoom);
            int fontSizePx = Math.round(text.getFontSizeInPt()/72*resolution*zoom);
            int marginTop = (int)((text.getYDirAdj())*zoom-fontSizePx);

            String fontString = "";
            PDFont font = text.getFont();
            PDFontDescriptor fontDescriptor = font.getFontDescriptor();

            if (fontDescriptor != null) {
                    fontString = fontDescriptor.getFontName();    			
            }
            else {
                    fontString = "";	
            }

            int indexPlus = fontString.indexOf("+");
            if (indexPlus != -1) {
                    fontString = fontString.substring(indexPlus+1);
            }
            boolean isBold = fontString.contains("Bold");
            boolean isItalic = fontString.contains("Italic");

            int indexDash = fontString.indexOf("-");
            if (indexDash != -1) {
                    fontString = fontString.substring(0, indexDash);
            }
            int indexComa = fontString.indexOf(",");
            if (indexComa != -1) {
                    fontString = fontString.substring(0, indexComa);
            }
            if(type == 2)
                renderingGroupByLineWithCache(text, marginLeft,  marginTop, fontSizePx, fontString, isBold, isItalic);   
            else if(type == 3)
                paragraphCreationWithLineBreak(text, marginLeft,  marginTop, fontSizePx, fontString, isBold, isItalic);
            else if(type == 5)
                paragraphCreationWithoutLineBreak(text, marginLeft,  marginTop, fontSizePx, fontString, isBold, isItalic);
            else if(type == 4)
                listExtractionFromTextposition(text, marginLeft,  marginTop, fontSizePx, fontString, isBold, isItalic);  
        } 
        catch (IOException e) 
        {	
        }    
    }	
    
//    StringBuffer sbForParagraph = new StringBuffer("<div align = ">");
    StringBuffer sbForParagraph = new StringBuffer("<div align = \"center\">");
    StringBuffer tempForParagraph = new StringBuffer();
    StringBuffer currentLineofParagraph = new StringBuffer();
    StringBuffer sbForList = new StringBuffer("<table>");
//    StringBuffer sbForList = new StringBuffer("<div align = \"center\"><table>");
    StringBuffer pTag = new StringBuffer("\">");
    
    public StringBuffer getParagraph()
    {
        int alignment = 0;//alignmentCheck(firstCharOfLineStartsAt,lastCharOfLineEndsAt);
        switch(alignment)
        {
            case 2:
                align = "</p><p style=\"text-align:justify;";
                break;
            case 0:
                align = "</p><p style=\"text-align:left;";
                break;
            case 1:
                align = "</p><p style=\"text-align:right;";
                break;
            case 3:
                align = "</p><p style=\"text-align:center;";
                break;
            default:
                align = "</p><p style=\"text-align:left;";
                break;
        }
        
        String closeBoldTag = "", closeItalicTag = "";
        
        if(lastIsItalic)
        {
            closeItalicTag = "</i>";
            lastIsItalic = false;
        }        
        if(lastIsBold)
        {
            closeBoldTag = "</b>";
            lastIsBold = false;
        }
        
        sbForParagraph.append(align).append(tempForParagraph);
        tempForParagraph = new StringBuffer();
        return sbForParagraph.append(closeItalicTag).append(closeBoldTag).append("</p></div>");
    }
    
    String regex = "[(a-zA-Z0-9]+[.)]";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher;
    int start = 0;
    int end = 0;
    
    private boolean matchPattern(String texts){
        
        matcher = pattern.matcher(texts);
        try{
            matcher = matcher.region(0, 10);
//            System.out.println("Matches: "+matcher.find());
        }
        catch(Exception ex)
        {
            return false;
        }
        if(matcher.find())
        {
            start = matcher.start();
            end = matcher.end();
            return true;
        }
//        System.out.println("Matches Starts at"+matcher.start()+"; and Ends at"+matcher.end());
        return false;
    }
    
    public StringBuffer getList()
    {
        String ss = sbForList.toString();
        StringBuffer sb = new StringBuffer(); 
        
//        Pattern pattern = Pattern.compile("[0-9iI]+[.)]+[ \t]");
//        System.out.println("Matches !!!!!!!!!!"+pattern.matcher(ss).groupCount());//+" "+pattern.matcher(ss).group(0).toString());
//        String repalaceThe = " o ",replaceWith = "</td></tr><tr><td></td><td>o&nbsp&nbsp&nbsp&nbsp";
//        ss = ss.replaceAll(repalaceThe,replaceWith);   
//        RegularExpression reg = new RegularExpression(regex,ss);
//        ss = ss.replaceAll("[a-zA-Z0-9iI]+[.)]+[ \t]", replaceWith);
        
        sb.append(ss).append("</td></tr></table>");
        return sb;
//        return sbForList.append("</td></tr></table>");
    }
    
    public void renderingGroupByLineWithCache16042013(TextPosition text, int marginLeft, int marginTop, int fontSizePx, String fontString, boolean isBold, boolean isItalic) throws IOException 
    {  
//        System.out.println("It comes here for: "+text.getCharacter());
        //<editor-fold defaultstate="collapsed" desc="IF">
        if (marginLeft-lastMarginRight> text.getWidthOfSpace())
        {
            currentLine.append(" ");
            sizeAllSpace += (marginLeft-lastMarginRight);
            numberSpace++;
            addSpace = false;
        }
        //</editor-fold>
        
        //<editor-fold defaultstate="collapsed" desc="IF">
        if (lastMarginTop != marginTop)
        {
            if (lastMarginTop != 0)
            {
                int spaceWidth = 4;
                if(paragraphcheck)
                {
                    sbForParagraph.append("<p style=\"word-spacing:").append(spaceWidth).append(";");
                }
                
                if("".equals(currentLine.toString()) || " ".equals(currentLine.toString()) || currentLine.toString()== null)
                { 
                    sbForParagraph.append(pTag).append("").append(currentParagraph);
                    currentParagraph = new StringBuffer();
                    if(!lastWasBold && !lastWasItalic)
                    {
                        sbForParagraph.append("</b></i>");
                        lastWasBold = true;
                        lastWasItalic = true;
                    }
                    else if(!lastWasBold)
                    {
                        sbForParagraph.append("</b>");
                        lastWasBold = true;
                    }
                    else if(!lastWasItalic)
                    {
                        sbForParagraph.append("</i>");
                        lastWasItalic = true;
                    }
                    sbForParagraph.append("</p>\n");
                    paragraphcheck = true;
                }
                else
                {
                    currentParagraph.append(currentLine.toString()).append(" ");
                    paragraphcheck = false;
                }
            }
            numberSpace = 0;
            sizeAllSpace = 0;
            currentLine = new StringBuffer();
            startXLine = marginLeft;
            lastMarginTop = marginTop;
            wasBold = isBold;
            wasItalic = isItalic;
            lastFontSizePx = fontSizePx;
            lastFontString = fontString;
            addSpace = false;
        }
        //</editor-fold>
        
        //<editor-fold defaultstate="collapsed" desc="ELSE">
        else
        {
            int sizeCurrentSpace = (int) (marginLeft-lastMarginRight-text.getWidthOfSpace());
            if (sizeCurrentSpace > 5)
            {
                if (lastMarginTop != 0)
                {
                    if (numberSpace != 0)
                    {
                        int spaceWidth = 4;
                        if(paragraphcheck)
                        {
                            sbForParagraph.append("<p style=\"word-spacing:").append(spaceWidth).append(";");
                        }
                    }
                    else
                    {
                        if(paragraphcheck)
                        {
                            sbForParagraph.append("<p style=\" "+";");
                        }
                    }
                    
                    if(("".equals(currentLine.toString()) || " ".equals(currentLine.toString()))&&(lastMarginTop!=marginTop))
                    {   
                        sbForParagraph.append(pTag).append(currentParagraph);
                        currentParagraph = new StringBuffer();
                        if(!lastWasBold && !lastWasItalic)
                        {
                            sbForParagraph.append("</b></i>");
                            lastWasBold = true;
                            lastWasItalic = true;
                        }
                        else if(!lastWasBold)
                        {
                            sbForParagraph.append("</b>");
                            lastWasBold = true;
                        }
                        else if(!lastWasItalic)
                        {
                            sbForParagraph.append("</i>");
                            lastWasItalic = true;
                        }
                        sbForParagraph.append("</p>\n");
                        paragraphcheck = true;
                    }
                    else
                    {
                        currentParagraph.append(currentLine.toString()).append(" ");
                        paragraphcheck = false;
                    }
                }
                numberSpace = 0;
                sizeAllSpace = 0;
                currentLine = new StringBuffer();
                startXLine = marginLeft;
                lastMarginTop = marginTop;
                wasBold = isBold;
                wasItalic = isItalic;
                lastFontSizePx = fontSizePx;
                lastFontString = fontString;
                addSpace = false;
            }
            else
            {
                if (addSpace)
                {
                    currentLine.append(" ");
                    sizeAllSpace += (marginLeft-lastMarginRight);
                    numberSpace++;
                    addSpace = false;
                }
            }
        }
        //</editor-fold>
        
        //<editor-fold defaultstate="collapsed" desc="IF-ELSE">
        if (text.getCharacter().equals(" "))
            addSpace = true;
        else
        {          
            try
            {
                if(wasBold && lastWasBold && wasItalic && lastWasItalic)
                {
                    currentLine.append("<b><i>");
                    lastWasBold = false;
                    lastWasItalic = false;
                }
                else if(wasBold && lastWasBold)
                {
                    currentLine.append("<b>");
                    lastWasBold = false;
                }
                else if(wasItalic && lastWasItalic)
                {
                    currentLine.append("<i>");
                    lastWasItalic = false;
                }
                else if(!wasBold && !lastWasBold && !wasItalic && !lastWasItalic)
                {
                    currentLine.append("</b></i>");
                    lastWasBold = true;
                    lastWasItalic = true;
                }
                else if(!wasBold && !lastWasBold)
                {
                    currentLine.append("</b>");
                    lastWasBold = true;
                }
                else if(!wasItalic && !lastWasItalic)
                {
                    currentLine.append("</i>");
                    lastWasItalic = true;
                }
                currentLine.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
            }
            catch(Exception e)
            {
//                System.out.println("Problems at Currentline: "+e);
            }
        }
        //</editor-fold>
			
        lastMarginLeft = marginLeft;
        lastMarginRight = (int) (marginLeft + text.getWidth()*zoom);
    }
 
    int firstCharPos = 0;
    int firstSymbolPosition = 0;
    int layerCount;// = 0;
    
    
    private void listExtractionFromTextposition(TextPosition text, int marginLeft, int marginTop, int fontSizePx, String fontString, boolean isBold, boolean isItalic) throws IOException 
    {
        isSymbol = false;
        String cellInHex = HexStringConverter.getHexStringConverterInstance().stringToHex(text.getCharacter());
//        System.out.println("Cell in Hexadecimal "+text.getCharacter()+":"+cellInHex);
        String singleCharacter = symbolCheck(cellInHex, text);
        if (lastMarginTop == marginTop) 
        {   			
            if (lastMarginLeft>marginLeft) 
            {    			
                sbForList.append("</td></tr><tr><td valign="+"\"top\""+">");
            }
            lastMarginTop = marginTop;
            sbForList.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
        }
        else 
        {
            if (lastMarginTop != 0 && isSymbol && layerCount == 0) 
            {  
                layerCount++;
                sbForList.append("</td></tr><tr><td valign=\"top\""+">");
                int difference = (int)(text.getX() - 80)/4;
                for(int i =0; i<difference;i++)
                {
                    sbForList.append("&nbsp");
                }
                sbForList.append(singleCharacter).append("</td><td>");
            }
            else if (lastMarginTop != 0 && isSymbol && (Math.abs((int)text.getX()-firstSymbolPosition)<3)) 
            {  
                sbForList.append("</td></tr><tr><td valign="+"\"top\""+">").append(singleCharacter).append("</td><td>");
            }
            else if (lastMarginTop != 0 && isSymbol) 
            {  
                sbForList.append("</td></tr><tr><td></td><td valign="+"\"top\""+">");
                int difference = (int)(text.getX() - 80)/4;
                for(int i =0; i<difference;i++)
                {
                    sbForList.append("&nbsp");
                }
                sbForList.append(singleCharacter);
            }
            else if(lastMarginTop!=0)
                sbForList.append(singleCharacter);
            else
            {
                sbForList.append("<tr><td valign="+"\"top\""+">");                
                if(isSymbol)
                {
                    layerCount++;
                    sbForList.append(singleCharacter).append("</td><td>");
                }
                else
                    sbForList.append(singleCharacter);
                firstCharPos = (int)text.getX();
            }			
            lastMarginTop = marginTop;		
        }
        
//        htmlFile.write(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
        lastMarginLeft = marginLeft;	
    }	
        
    
    private void renderingGroupByLineWithCache(TextPosition text, int marginLeft, int marginTop, int fontSizePx, String fontString, boolean isBold, boolean isItalic) throws IOException 
    {    
        String cellInHex = HexStringConverter.getHexStringConverterInstance().stringToHex(text.getCharacter());
        if (marginLeft-lastMarginRight> text.getWidthOfSpace()) 
        {    		
            currentLine.append(" "); 
            sizeAllSpace += (marginLeft-lastMarginRight);
            numberSpace++;
            addSpace = false;
    	}
    	if ((lastMarginTop != marginTop) || (!lastFontString.equals(fontString)) || (wasBold != isBold) || (wasItalic != isItalic) || (lastFontSizePx != fontSizePx) || (lastMarginLeft>marginLeft) || (marginLeft-lastMarginRight>150))
        {
            if (lastMarginTop != 0) 
            {				
                boolean display = true;				
                // if the bloc is empty, we do not display it (for a lighter result)
                if (currentLine.length() == 1) 
                {					
                    char firstChar = currentLine.charAt(0);
                    if (firstChar == ' ') 
                    {
                            display = false;
                    }
                }
                if (display) 
                {
                    if (numberSpace != 0) 
                    {
                        int spaceWidth = Math.round(((float) sizeAllSpace)/((float) numberSpace)-text.getWidthOfSpace());    
                        htmlFile.write("<p style=\"word-spacing:"+0+"px; margin-left:"+startXLine+"px; font-size: "+lastFontSizePx+"px; font-family:"+lastFontString+";");
                    }
                    else 
                    {                		
                        htmlFile.write("<p style=\"margin-left:"+startXLine+"px; font-size: "+lastFontSizePx+"px; font-family:"+lastFontString+";");
                    }
                    if (wasBold) 
                    {            			
                        htmlFile.write("font-weight: bold;");            		
                    }            		
                    if (wasItalic)                         
                    {            			
                        htmlFile.write("font-style: italic;");            		
                    }        			
                    htmlFile.write("\">");   
                    htmlFile.write(currentLine.toString());
//                    System.out.println(currentLine.toString());
                    htmlFile.write("</p>\n");
                }			
            }   			
            numberSpace = 0;   			
            sizeAllSpace = 0;
            currentLine = new StringBuffer();
            startXLine = marginLeft;
            lastMarginTop = marginTop;
            wasBold = isBold;
            wasItalic = isItalic;
            lastFontSizePx = fontSizePx;
            lastFontString = fontString;
            addSpace = false;		
        }		
        else 
        {
            int sizeCurrentSpace = (int) (marginLeft-lastMarginRight-text.getWidthOfSpace());
            if (sizeCurrentSpace > 5) 
            {
                if (lastMarginTop != 0) 
                {
                    if (numberSpace != 0) 
                    {
                        int spaceWidth = Math.round(((float) sizeAllSpace)/((float) numberSpace)-text.getWidthOfSpace());                		
                        htmlFile.write("<p style=\"word-spacing:"+0+"px; margin-left:"+startXLine+"px; font-size: "+lastFontSizePx+"px; font-family:"+lastFontString+";");        			
                    }
                    else 
                    {
                        htmlFile.write("<p style=\" margin-left:"+startXLine+"px; font-size: "+lastFontSizePx+"px; font-family:"+lastFontString+";");        			
                    }
                    if (wasBold) 
                    {
                        htmlFile.write("font-weight: bold;");
                    }
            		
                    if (wasItalic) 
                    {                            
                        htmlFile.write("font-style: italic;");
                    }
                    htmlFile.write("\">");
                    htmlFile.write(currentLine.toString());
//                    System.out.println(currentLine.toString());
                    htmlFile.write("</p>\n");    			
                }       			
                numberSpace = 0;
                sizeAllSpace = 0;
                currentLine = new StringBuffer();
                startXLine = marginLeft;
                lastMarginTop = marginTop;
                wasBold = isBold;
                wasItalic = isItalic;
                lastFontSizePx = fontSizePx;
                lastFontString = fontString;
                addSpace = false;    		
            }
            else 
            {
                if (addSpace) 
                {
                    currentLine.append(" ");
                    sizeAllSpace += (marginLeft-lastMarginRight);
                    numberSpace++;
                    addSpace = false;   	   			
                }	
            }
        }
        if (text.getCharacter().equals(" ")) 
        {
            addSpace = true;
            //sizeAllSpace += text.getWidthOfSpace();
        }
        else 
        {
            if("e280a2".equals(cellInHex))
                currentLine.append("&bull;");
            else if("ef82b7".equals(cellInHex))
                currentLine.append("&bull;");  
            else if("ef82a7".equals(cellInHex))
                currentLine.append("&#9632;");
            else if("ef8398".equals(cellInHex))
                currentLine.append("&#f0d8;");
            else
                currentLine.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
        }			
        lastMarginLeft = marginLeft;
        lastMarginRight = (int) (marginLeft + text.getWidth()*zoom);	
    } 
    
    boolean lastIsBold = false;
    boolean lastIsItalic = false;
    
    public void paragraphCreationWithLineBreak1(TextPosition text, int marginLeft, int marginTop, int fontSizePx, String fontString, boolean isBold, boolean isItalic) throws IOException 
    {
        if (lastMarginTop == marginTop) 
        {
            if (isBold && !lastIsBold ) 
            {
                tempForParagraph.append("<b>");
                lastIsBold = true;
            }
            if (isItalic && !lastIsItalic)
            {
                tempForParagraph.append("<i>");
                lastIsItalic = true;
            }
            if (!isItalic && lastIsItalic)
            {
                tempForParagraph.append("</i>");
                lastIsBold = false;
            }
            if (lastIsBold && !isBold)
            {
                tempForParagraph.append("</b>");
                lastIsBold = false;
            }
            
            currentLineofParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
            tempForParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
            lastMarginLeft = marginLeft;
            lastMarginTop = marginTop;		
            lastMarginRight = (int) (marginLeft + text.getWidth()*zoom);
        }
        else 
        {
            lastMarginRight = (int) (marginLeft + text.getWidth()*zoom);
            if (lastMarginTop == 0) 
            {
                tempForParagraph.append("font-size: ").append(fontSizePx).append("px; font-family:").append(fontString).append(";\">");
                if (isBold) 
                {
                    tempForParagraph.append("<b>");
                    lastIsBold = true;
                }
                if (isItalic)
                {
                    tempForParagraph.append("<i>");
                    lastIsItalic = true;
                }
                tempForParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
                currentLineofParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
                lastMarginLeft = marginLeft;
                firstCharOfLineStartsAt.add(lastMarginLeft);
            }
            else if(marginTop>lastMarginTop && (currentLineofParagraph.length()<3|| Math.abs(marginTop-lastMarginTop - text.getHeight()*zoom)>=24)) /// When found a line with empty characters.
            {
//                System.out.println("Difference found: "+Math.abs(marginTop-lastMarginTop-text.getHeight()*zoom)+"Margin Top:"+marginTop+", Last margin Top: "+lastMarginTop+" at Character"+ text.getCharacter());
                lastCharOfLineEndsAt.add(lastMarginRight);
                pCount++;
                int alignment = alignmentCheck(firstCharOfLineStartsAt,lastCharOfLineEndsAt);
                
                if(pCount==1)
                {
                    switch(alignment)
                    {
                        case 2:
                            align = "<p style=\"text-align:justify;";
                            break;
                        case 0:
                            align = "<p style=\"text-align:left;";
                            break;
                        case 1:
                            align = "<p style=\"text-align:right;";
                            break;
                        case 3:
                            align = "<p style=\"text-align:center;";
                            break;
                        default:
                            align = "<p style=\"text-align:left;";
                            break;
                    }
                }
                else
                {
                    switch(alignment)
                    {
                        case 2:
                            align = "</p><p style=\"text-align:justify;";
                            break;
                        case 0:
                            align = "</p><p style=\"text-align:left;";
                            break;
                        case 1:
                            align = "</p><p style=\"text-align:right;";
                            break;
                        case 3:
                            align = "</p><p style=\"text-align:center;";
                            break;
                        default:
                            align = "</p><p style=\"text-align:left;";
                            break;
                    }
                }
                String closeBoldTag = "",closeItalicTag = "";
                if(lastIsItalic)
                {
                    closeItalicTag = "</i>";
                    lastIsItalic = false;
                }
                if(lastIsBold)
                {
                    closeBoldTag = "</b>";
                    lastIsBold = false;
                }
                sbForParagraph.append(closeItalicTag).append(closeBoldTag).append(align).append(tempForParagraph);
                tempForParagraph = new StringBuffer();
                currentLineofParagraph = new StringBuffer();
                firstCharOfLineStartsAt = new ArrayList<Integer>();
                lastCharOfLineEndsAt = new ArrayList<Integer>();
                tempForParagraph.append("font-size: ").append(fontSizePx).append("px; font-family:").append(fontString).append(";\">");
                if (isBold && !lastIsBold ) 
                {
                    tempForParagraph.append("<b>");
                    lastIsBold = true;
                }
                if (isItalic && !lastIsItalic)
                {
                    tempForParagraph.append("<i>");
                    lastIsItalic = true;
                }
                if (!isItalic && lastIsItalic)
                {
                    tempForParagraph.append("</i>");
                    lastIsBold = false;
                }
                
                if (lastIsBold && !isBold)
                {
                    tempForParagraph.append("</b>");
                    lastIsBold = false;
                }
                tempForParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
                currentLineofParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
                lastMarginLeft = marginLeft;
                firstCharOfLineStartsAt.add(lastMarginLeft);
            }
            else if(marginTop>lastMarginTop)
            {
                currentLineofParagraph = new StringBuffer();
//                System.out.println("Difference found at not greater than 25 and marginTop>lastMarginTop: "+Math.abs(marginTop-lastMarginTop-text.getHeight()*zoom)+"Margin Top:"+marginTop+", Last margin Top: "+lastMarginTop+" at Character"+ text.getCharacter());
                if (isBold && !lastIsBold ) 
                {
                    tempForParagraph.append("<b>");
                    lastIsBold = true;
                }
                if (isItalic && !lastIsItalic)
                {
                    tempForParagraph.append("<i>");
                    lastIsItalic = true;
                }
                if (!isItalic && lastIsItalic)
                {
                    tempForParagraph.append("</i>");
                    lastIsBold = false;
                }
                
                if (lastIsBold && !isBold)
                {
                    tempForParagraph.append("</b>");
                    lastIsBold = false;
                }
                tempForParagraph.append("<br>"+text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));    
                currentLineofParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
                lastMarginLeft = marginLeft;
                firstCharOfLineStartsAt.add(lastMarginLeft);
                lastCharOfLineEndsAt.add(lastMarginRight);
            }
            else
            {
//                System.out.println("Difference found at not greater than 25: "+Math.abs(marginTop-lastMarginTop-text.getHeight()*zoom)+"Margin Top:"+marginTop+", Last margin Top: "+lastMarginTop+" at Character"+ text.getCharacter());
                tempForParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
                currentLineofParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
                lastMarginLeft = marginLeft; 
            }            
            lastMarginTop = marginTop;
        }        
    }
    
    /**
     *
     * @param text
     * @param marginLeft
     * @param marginTop
     * @param fontSizePx
     * @param fontString
     * @param isBold
     * @param isItalic
     * @throws IOException
     */
    public void appendCharacter(TextPosition text)
    {
        int charInDecimal = (int)text.getCharacter().toCharArray()[0];
        if(charInDecimal>255)
        {
            currentLineofParagraph.append("&#").append((int)text.getCharacter().toCharArray()[0]).append(";");
            tempForParagraph.append("&#").append((int)text.getCharacter().toCharArray()[0]).append(";");
        }
        else
        {
            if(charInDecimal == 0)
            {
                
            }
//            else if(charInDecimal == 32)
//            {
//                tempForParagraph.append("&#32;");
//                currentLineofParagraph.append("&#32;");
//            }
            else if(charInDecimal == 38)
            {
                tempForParagraph.append("&#38;");
                currentLineofParagraph.append("&#38;");
            }
            else
            {
                tempForParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
                currentLineofParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
            }
            
        }
    }
    
    
    public void paragraphCreationWithLineBreak(TextPosition text, int marginLeft, int marginTop, int fontSizePx, String fontString, boolean isBold, boolean isItalic) throws IOException 
    {
        if (lastMarginTop == marginTop)         
        {
            if (isBold && !lastIsBold ) 
                {
                    tempForParagraph.append("<b>");
                    lastIsBold = true;
                }
                if (isItalic && !lastIsItalic)
                {
                    tempForParagraph.append("<i>");
                    lastIsItalic = true;
                }
                if (!isItalic && lastIsItalic)
                {
                    tempForParagraph.append("</i>");
                    lastIsBold = false;
                }
                
                if (lastIsBold && !isBold)
                {
                    tempForParagraph.append("</b>");
                    lastIsBold = false;
                }
            appendCharacter(text);
//            if((int)text.getCharacter().toCharArray()[0]>255)
//            {
//                currentLineofParagraph.append("&#").append((int)text.getCharacter().toCharArray()[0]).append(";");
//                tempForParagraph.append("&#").append((int)text.getCharacter().toCharArray()[0]).append(";");
//            }
//            else
//            {                
//                currentLineofParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
//                tempForParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
//            }
            lastMarginLeft = marginLeft;
            lastMarginTop = marginTop;		
            lastMarginRight = (int) (marginLeft + text.getWidth()*zoom);
        }
        else 
        {
            lastMarginRight = (int) (marginLeft + text.getWidth()*zoom);
            if (lastMarginTop == 0) 
            {
                tempForParagraph.append("font-size: ").append(fontSizePx).append("px; font-family:").append(fontString).append(";\">");
                if (isBold) 
                {
                    tempForParagraph.append("<b>");
                    lastIsBold = true;
                }
                if (isItalic)
                {
                    tempForParagraph.append("<i>");
                    lastIsItalic = true;
                }
                appendCharacter(text);
//                if((int)text.getCharacter().toCharArray()[0]>255)
//                {
//                    currentLineofParagraph.append("&#").append((int)text.getCharacter().toCharArray()[0]).append(";");
//                    tempForParagraph.append("&#").append((int)text.getCharacter().toCharArray()[0]).append(";");
//                }
//                else
//                {                
//                    currentLineofParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
//                    tempForParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
//                }
                lastMarginLeft = marginLeft;
                firstCharOfLineStartsAt.add(lastMarginLeft);
            }
            else if(marginTop>lastMarginTop && (currentLineofParagraph.length()<3|| Math.abs(marginTop-lastMarginTop-text.getHeight()*zoom)>=24)) /// When found a line with empty characters.
            {
//                System.out.println("Difference found: "+Math.abs(marginTop-lastMarginTop-text.getHeight()*zoom)+"Margin Top:"+marginTop+", Last margin Top: "+lastMarginTop+" at Character"+ text.getCharacter());
                lastCharOfLineEndsAt.add(lastMarginRight);
                pCount++;
                int alignment = alignmentCheck(firstCharOfLineStartsAt,lastCharOfLineEndsAt);
                
                if(pCount==1)
                {
                    switch(alignment)
                    {
                        case 2:
                            align = "<p style=\"text-align:justify;";
                            break;
                        case 0:
                            align = "<p style=\"text-align:left;";
                            break;
                        case 1:
                            align = "<p style=\"text-align:right;";
                            break;
                        case 3:
                            align = "<p style=\"text-align:center;";
                            break;
                        default:
                            align = "<p style=\"text-align:left;";
                            break;
                    }
                }
                else
                {
                    String closeBoldTag = "",closeItalicTag = "";
                    if(lastIsItalic)
                    {
                        closeItalicTag = "</i>";
                        lastIsItalic = false;
                    }
                    if(lastIsBold)
                    {
                        closeBoldTag = "</b>";
                        lastIsBold = false;
                    }
                    tempForParagraph.append(closeItalicTag).append(closeBoldTag);
                    switch(alignment)
					{
                        case 2:
                            align = "</p><p style=\"text-align:justify;";
                            break;
                        case 0:
                            align = "</p><p style=\"text-align:left;";
                            break;
                        case 1:
                            align = "</p><p style=\"text-align:right;";
                            break;
                        case 3:
                            align = "</p><p style=\"text-align:center;";
                            break;
                        default:
                            align = "</p><p style=\"text-align:left;";
                            break;
                    }
                }
                
                sbForParagraph.append(align).append(tempForParagraph);
                tempForParagraph = new StringBuffer();
                currentLineofParagraph = new StringBuffer();
                firstCharOfLineStartsAt = new ArrayList<Integer>();
                lastCharOfLineEndsAt = new ArrayList<Integer>();
                tempForParagraph.append("font-size: ").append(fontSizePx).append("px; font-family:").append(fontString).append(";\">");
                if (isBold && !lastIsBold ) 
                {
                    tempForParagraph.append("<b>");
                    lastIsBold = true;
                }
                if (isItalic && !lastIsItalic)
                {
                    tempForParagraph.append("<i>");
                    lastIsItalic = true;
                }
                if (!isItalic && lastIsItalic)
                {
                    tempForParagraph.append("</i>");
                    lastIsItalic = false;
                }
                if (!isBold && lastIsBold )
                {
                    tempForParagraph.append("</b>");
                    lastIsBold = false;
                }
                
                appendCharacter(text);
//                if((int)text.getCharacter().toCharArray()[0]>255)
//                {
//                    currentLineofParagraph.append("&#").append((int)text.getCharacter().toCharArray()[0]).append(";");
//                    tempForParagraph.append("&#").append((int)text.getCharacter().toCharArray()[0]).append(";");
//                }
//                else
//                {                
//                    currentLineofParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
//                    tempForParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
//                }
                lastMarginLeft = marginLeft;
                firstCharOfLineStartsAt.add(lastMarginLeft);
            }
            else if(marginTop>lastMarginTop)
            {
                currentLineofParagraph = new StringBuffer();
//                System.out.println("Difference found at not greater than 25 and marginTop>lastMarginTop: "+Math.abs(marginTop-lastMarginTop-text.getHeight()*zoom)+"Margin Top:"+marginTop+", Last margin Top: "+lastMarginTop+" at Character"+ text.getCharacter());
                if (isBold && !lastIsBold ) 
                {
                    tempForParagraph.append("<b>");
                    lastIsBold = true;
                }
                if (isItalic && !lastIsItalic)
                {
                    tempForParagraph.append("<i>");
                    lastIsItalic = true;
                }
                if (!isItalic && lastIsItalic)
                {
                    tempForParagraph.append("</i>");
                    lastIsItalic = false;
                }                
                if (!isBold && lastIsBold)
                {
                    tempForParagraph.append("</b>");
                    lastIsBold = false;
                }
                tempForParagraph.append("<br/>");
                appendCharacter(text);
//                if((int)text.getCharacter().toCharArray()[0]>255)
//                {
//                    currentLineofParagraph.append("&#").append((int)text.getCharacter().toCharArray()[0]).append(";");
//                    tempForParagraph.append("&#").append((int)text.getCharacter().toCharArray()[0]).append(";");
//                }
//                else
//                {                
//                    currentLineofParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
//                    tempForParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
//                }
                lastMarginLeft = marginLeft;
                firstCharOfLineStartsAt.add(lastMarginLeft);
                lastCharOfLineEndsAt.add(lastMarginRight);
            }
            else
            {
                if (isBold && !lastIsBold ) 
                {
                    tempForParagraph.append("<b>");
                    lastIsBold = true;
                }
                if (isItalic && !lastIsItalic)
                {
                    tempForParagraph.append("<i>");
                    lastIsItalic = true;
                }
                if (!isItalic && lastIsItalic)
                {
                    tempForParagraph.append("</i>");
                    lastIsItalic = false;
                }                
                if (!isBold && lastIsBold)
                {
                    tempForParagraph.append("</b>");
                    lastIsBold = false;
                }
                appendCharacter(text);
//                System.out.println("Difference found at not greater than 25: "+Math.abs(marginTop-lastMarginTop-text.getHeight()*zoom)+"Margin Top:"+marginTop+", Last margin Top: "+lastMarginTop+" at Character"+ text.getCharacter());
//                tempForParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
//                currentLineofParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
                lastMarginLeft = marginLeft; 
            }            
            lastMarginTop = marginTop;
        }        
    }
        
    public void paragraphCreationWithoutLineBreak1(TextPosition text, int marginLeft, int marginTop, int fontSizePx, String fontString, boolean isBold, boolean isItalic) throws IOException 
    {
        if (lastMarginTop == marginTop) 
        {
            if (isBold && !lastIsBold ) 
            {
                tempForParagraph.append("<b>");
                lastIsBold = true;
            }
            else if (lastIsBold && !isBold)
            {
                tempForParagraph.append("</b>");
                lastIsBold = false;
            }
            if (isItalic && !lastIsItalic)
            {
                tempForParagraph.append("<i>");
                lastIsItalic = true;
            }
            else if (!isItalic && lastIsItalic)
            {
                tempForParagraph.append("</i>");
                lastIsItalic = false;
            }
            currentLineofParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
            tempForParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
            lastMarginLeft = marginLeft;
            lastMarginTop = marginTop;		
            lastMarginRight = (int) (marginLeft + text.getWidth()*zoom);
        }
        else 
        {
            lastMarginRight = (int) (marginLeft + text.getWidth()*zoom);
            if (lastMarginTop == 0) 
            {
                tempForParagraph.append("font-size: ").append(fontSizePx).append("px; font-family:").append(fontString).append(";\">");
                if (isBold) 
                {
                    tempForParagraph.append("<b>");
                    lastIsBold = true;
                }
                if (isItalic)
                {
                    tempForParagraph.append("<i>");
                    lastIsItalic = true;
                }
                tempForParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
                currentLineofParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
                lastMarginLeft = marginLeft;
                firstCharOfLineStartsAt.add(lastMarginLeft);
            }
            else if(marginTop>lastMarginTop && (currentLineofParagraph.length()<3|| Math.abs(marginTop-lastMarginTop-text.getHeight()*zoom)>=24)) /// When found a line with empty characters.
            {
//                System.out.println("Difference found: "+Math.abs(marginTop-lastMarginTop-text.getHeight()*zoom)+"Margin Top:"+marginTop+", Last margin Top: "+lastMarginTop+" at Character"+ text.getCharacter());
                lastCharOfLineEndsAt.add(lastMarginRight);
                pCount++;
                int alignment = alignmentCheck(firstCharOfLineStartsAt,lastCharOfLineEndsAt);
                
                if(pCount==1)
                {
                    switch(alignment)
                    {
                        case 2:
                            align = "<p style=\"text-align:justify;";
                            break;
                        case 0:
                            align = "<p style=\"text-align:left;";
                            break;
                        case 1:
                            align = "<p style=\"text-align:right;";
                            break;
                        case 3:
                            align = "<p style=\"text-align:center;";
                            break;
                        default:
                            align = "<p style=\"text-align:left;";
                            break;
                    }
                }
                else
                {
                    String closeBoldTag = "",closeItalicTag = "";
//                    if(lastIsBold)
//                    {
//                        closeBoldTag = "</b>";
//                        lastIsBold = false;
//                    }
//                    if(lastIsItalic)
//                    {
//                        closeItalicTag = "</i>";
//                        lastIsItalic = false;
//                    }
                    if (isBold && !lastIsBold ) 
                    {
                        closeBoldTag = "<b>";
                        lastIsBold = true;
                    }
                    else if (!isBold && lastIsBold )
                    {
                        closeBoldTag = "</b>";
                        lastIsBold = false;
                    }
                    if (isItalic && !lastIsItalic)
                    {
                        closeItalicTag = "<i>";
                        lastIsItalic = true;
                    }
                    else if (!isItalic && lastIsItalic)
                    {
                        closeItalicTag = "</i>";
                        lastIsItalic = false;
                    }
                    sbForParagraph.append(closeItalicTag).append(closeBoldTag);
                    switch(alignment)
                    {
                        case 2:
                            align = "</p><p style=\"text-align:justify;";
                            break;
                        case 0:
                            align = "</p><p style=\"text-align:left;";
                            break;
                        case 1:
                            align = "</p><p style=\"text-align:right;";
                            break;
                        case 3:
                            align = "</p><p style=\"text-align:center;";
                            break;
                        default:
                            align = "</p><p style=\"text-align:left;";
                            break;
                    }
                }
                
                sbForParagraph.append(align).append(tempForParagraph);
                tempForParagraph = new StringBuffer();
                currentLineofParagraph = new StringBuffer();
                firstCharOfLineStartsAt = new ArrayList<Integer>();
                lastCharOfLineEndsAt = new ArrayList<Integer>();
                tempForParagraph.append("font-size: ").append(fontSizePx).append("px; font-family:").append(fontString).append(";\">");
                if (isBold && !lastIsBold ) 
                {
                    tempForParagraph.append("<b>");
                    lastIsBold = true;
                }
                else if (!isBold && lastIsBold )
                {
                    tempForParagraph.append("</b>");
                    lastIsBold = false;
                }
                if (isItalic && !lastIsItalic)
                {
                    tempForParagraph.append("<i>");
                    lastIsItalic = true;
                }
                else if (!isItalic && lastIsItalic)
                {
                    tempForParagraph.append("</i>");
                    lastIsItalic = false;
                }
                tempForParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
                currentLineofParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
                lastMarginLeft = marginLeft;
                firstCharOfLineStartsAt.add(lastMarginLeft);
            }
            else if(marginTop>lastMarginTop)
            {
                currentLineofParagraph = new StringBuffer();
//                System.out.println("Difference found at not greater than 25 and marginTop>lastMarginTop: "+Math.abs(marginTop-lastMarginTop-text.getHeight()*zoom)+"Margin Top:"+marginTop+", Last margin Top: "+lastMarginTop+" at Character"+ text.getCharacter());
                if (isBold && !lastIsBold ) 
                {
                    tempForParagraph.append("<b>");
                    lastIsBold = true;
                }
                else if (!isBold && lastIsBold)
                {
                    tempForParagraph.append("</b>");
                    lastIsBold = false;
                }
                if (isItalic && !lastIsItalic)
                {
                    tempForParagraph.append("<i>");
                    lastIsItalic = true;
                }
                else if (!isItalic && lastIsItalic)
                {
                    tempForParagraph.append("</i>");
                    lastIsItalic = false;
                }
                tempForParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));    
                currentLineofParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
                lastMarginLeft = marginLeft;
                firstCharOfLineStartsAt.add(lastMarginLeft);
                lastCharOfLineEndsAt.add(lastMarginRight);
            }
            else
            {
//                System.out.println("Difference found at not greater than 25: "+Math.abs(marginTop-lastMarginTop-text.getHeight()*zoom)+"Margin Top:"+marginTop+", Last margin Top: "+lastMarginTop+" at Character"+ text.getCharacter());
                tempForParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
                currentLineofParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
                lastMarginLeft = marginLeft; 
            }            
            lastMarginTop = marginTop;
        }        
    }
    
    public void paragraphCreationWithoutLineBreak(TextPosition text, int marginLeft, int marginTop, int fontSizePx, String fontString, boolean isBold, boolean isItalic) throws IOException 
    {
//        System.out.println("Text :"+text.getCharacter()+" and its Props : Font Size:"+fontSizePx+" Font: "+fontString+" Is Bold: "+isBold+" Is Italic :"+isItalic);
        if (lastMarginTop == marginTop) 
        {
            if (isBold && !lastIsBold ) 
                {
                    tempForParagraph.append("<b>");
                    lastIsBold = true;
                }
                if (isItalic && !lastIsItalic)
                {
                    tempForParagraph.append("<i>");
                    lastIsItalic = true;
                }
                if (!isItalic && lastIsItalic)
                {
                    tempForParagraph.append("</i>");
                    lastIsItalic = false;
                }                
                if (!isBold && lastIsBold)
                {
                    tempForParagraph.append("</b>");
                    lastIsBold = false;
                }
            appendCharacter(text);
//            if((int)text.getCharacter().toCharArray()[0]>255)
//            {
//                currentLineofParagraph.append("&#").append((int)text.getCharacter().toCharArray()[0]).append(";");
//                tempForParagraph.append("&#").append((int)text.getCharacter().toCharArray()[0]).append(";");
//            }
//            else
//            {                
//                currentLineofParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
//                tempForParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
//            }
            lastMarginLeft = marginLeft;
            lastMarginTop = marginTop;		
            lastMarginRight = (int) (marginLeft + text.getWidth()*zoom);
        }
        else 
        {
            lastMarginRight = (int) (marginLeft + text.getWidth()*zoom);
            if (lastMarginTop == 0) 
            {
                tempForParagraph.append("font-size: ").append(fontSizePx).append("px; font-family:").append(fontString).append(";\">");
                if (isBold) 
                {
                    tempForParagraph.append("<b>");
                    lastIsBold = true;
                }
                if (isItalic)
                {
                    tempForParagraph.append("<i>");
                    lastIsItalic = true;
                }
                appendCharacter(text);
//                if((int)text.getCharacter().toCharArray()[0]>255)
//                {
//                    currentLineofParagraph.append("&#").append((int)text.getCharacter().toCharArray()[0]).append(";");
//                    tempForParagraph.append("&#").append((int)text.getCharacter().toCharArray()[0]).append(";");
//                }
//                else
//                {                
//                    currentLineofParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
//                    tempForParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
//                }
                lastMarginLeft = marginLeft;
                firstCharOfLineStartsAt.add(lastMarginLeft);
            }
            else if(marginTop>lastMarginTop && (currentLineofParagraph.length()<3|| Math.abs(marginTop-lastMarginTop-text.getHeight()*zoom)>=24)) /// When found a line with empty characters.
            {
//                System.out.println("Difference found: "+Math.abs(marginTop-lastMarginTop-text.getHeight()*zoom)+"Margin Top:"+marginTop+", Last margin Top: "+lastMarginTop+" at Character"+ text.getCharacter());
                lastCharOfLineEndsAt.add(lastMarginRight);
                pCount++;
                int alignment = alignmentCheck(firstCharOfLineStartsAt,lastCharOfLineEndsAt);
                
                if(pCount==1)
                {
                    switch(alignment)
                    {
                        case 2:
                            align = "<p style=\"text-align:justify;";
                            break;
                        case 0:
                            align = "<p style=\"text-align:left;";
                            break;
                        case 1:
                            align = "<p style=\"text-align:right;";
                            break;
                        case 3:
                            align = "<p style=\"text-align:center;";
                            break;
                        default:
                            align = "<p style=\"text-align:left;";
                            break;
                    }
                }
                else
                {
                    String closeBoldTag = "",closeItalicTag = "";
                    if(lastIsItalic)
                    {
                        closeItalicTag = "</i>";
                        lastIsItalic = false;
                    }
                    if(lastIsBold)
                    {
                        closeBoldTag = "</b>";
                        lastIsBold = false;
                    }
//                    if (isBold && !lastIsBold ) 
//                    {
//                        closeBoldTag = "<b>";
//                        lastIsBold = true;
//                    }
//                    else if (!isBold && lastIsBold )
//                    {
//                        closeBoldTag = "</b>";
//                        lastIsBold = false;
//                    }
//                    if (isItalic && !lastIsItalic)
//                    {
//                        closeItalicTag = "<i>";
//                        lastIsItalic = true;
//                    }
//                    else if (!isItalic && lastIsItalic)
//                    {
//                        closeItalicTag = "</i>";
//                        lastIsItalic = false;
//                    }
                    tempForParagraph.append(closeItalicTag).append(closeBoldTag);
                    switch(alignment)
                    {
                        case 2:
                            align = "</p><p style=\"text-align:justify;";
                            break;
                        case 0:
                            align = "</p><p style=\"text-align:left;";
                            break;
                        case 1:
                            align = "</p><p style=\"text-align:right;";
                            break;
                        case 3:
                            align = "</p><p style=\"text-align:center;";
                            break;
                        default:
                            align = "</p><p style=\"text-align:left;";
                            break;
                    }
                }
                
                sbForParagraph.append(align).append(tempForParagraph);
                tempForParagraph = new StringBuffer();
                currentLineofParagraph = new StringBuffer();
                firstCharOfLineStartsAt = new ArrayList<Integer>();
                lastCharOfLineEndsAt = new ArrayList<Integer>();
                tempForParagraph.append("font-size: ").append(fontSizePx).append("px; font-family:").append(fontString).append(";\">");
                if (isBold && !lastIsBold ) 
                {
                    tempForParagraph.append("<b>");
                    lastIsBold = true;
                }
                if (isItalic && !lastIsItalic)
                {
                    tempForParagraph.append("<i>");
                    lastIsItalic = true;
                }
                if (!isItalic && lastIsItalic)
                {
                    tempForParagraph.append("</i>");
                    lastIsItalic = false;
                }                
                if (!isBold && lastIsBold )
                {
                    tempForParagraph.append("</b>");
                    lastIsBold = false;
                }
                appendCharacter(text);
//                if((int)text.getCharacter().toCharArray()[0]>255)
//                {
//                    currentLineofParagraph.append("&#").append((int)text.getCharacter().toCharArray()[0]).append(";");
//                    tempForParagraph.append("&#").append((int)text.getCharacter().toCharArray()[0]).append(";");
//                }
//                else
//                {                
//                    currentLineofParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
//                    tempForParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
//                }
                lastMarginLeft = marginLeft;
                firstCharOfLineStartsAt.add(lastMarginLeft);
            }
            else if(marginTop>lastMarginTop)
            {
                currentLineofParagraph = new StringBuffer();
//                System.out.println("Difference found at not greater than 25 and marginTop>lastMarginTop: "+Math.abs(marginTop-lastMarginTop-text.getHeight()*zoom)+"Margin Top:"+marginTop+", Last margin Top: "+lastMarginTop+" at Character"+ text.getCharacter());
                if (isBold && !lastIsBold ) 
                {
                    tempForParagraph.append("<b>");
                    lastIsBold = true;
                }
                if (isItalic && !lastIsItalic)
                {
                    tempForParagraph.append("<i>");
                    lastIsItalic = true;
                }
                if (!isItalic && lastIsItalic)
                {
                    tempForParagraph.append("</i>");
                    lastIsItalic = false;
                }                
                if (!isBold && lastIsBold)
                {
                    tempForParagraph.append("</b>");
                    lastIsBold = false;
                }
                appendCharacter(text);
//                if((int)text.getCharacter().toCharArray()[0]>255)
//                {
//                    currentLineofParagraph.append("&#").append((int)text.getCharacter().toCharArray()[0]).append(";");
//                    tempForParagraph.append("&#").append((int)text.getCharacter().toCharArray()[0]).append(";");
//                }
//                else
//                {                
//                    currentLineofParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
//                    tempForParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
//                }
                lastMarginLeft = marginLeft;
                firstCharOfLineStartsAt.add(lastMarginLeft);
                lastCharOfLineEndsAt.add(lastMarginRight);
            }
            else
            {
                if (isBold && !lastIsBold ) 
                {
                    tempForParagraph.append("<b>");
                    lastIsBold = true;
                }
                if (isItalic && !lastIsItalic)
                {
                    tempForParagraph.append("<i>");
                    lastIsItalic = true;
                }
                if (!isItalic && lastIsItalic)
                {
                    tempForParagraph.append("</i>");
                    lastIsItalic = false;
                }                
                if (!isBold && lastIsBold)
                {
                    tempForParagraph.append("</b>");
                    lastIsBold = false;
                }
                appendCharacter(text);
//                System.out.println("Difference found at not greater than 25: "+Math.abs(marginTop-lastMarginTop-text.getHeight()*zoom)+"Margin Top:"+marginTop+", Last margin Top: "+lastMarginTop+" at Character"+ text.getCharacter());
//                if((int)text.getCharacter().toCharArray()[0]>255)
//                {
//                    currentLineofParagraph.append("&#").append((int)text.getCharacter().toCharArray()[0]).append(";");
//                    tempForParagraph.append("&#").append((int)text.getCharacter().toCharArray()[0]).append(";");
//                }
//                else
//                {                
//                    currentLineofParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
//                    tempForParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
//                }
                lastMarginLeft = marginLeft; 
            }            
            lastMarginTop = marginTop;
        }        
    }
        
    public void endOfTable() throws IOException{
        htmlFile.write("</td></tr></table></body></html>");
    }
    
    boolean isSymbol = false;
    /**
     *
     * @param cellInHex
     * @param text
     * @return
     */
    public String symbolCheck(String cellInHex, TextPosition text){
        if("e280a2".equals(cellInHex))
        {
            isSymbol = true;
            if(firstSymbolPosition ==0)
                firstSymbolPosition = (int)text.getX();
            return "&bull;&nbsp&nbsp&nbsp&nbsp"; 
        }
        else if("ef82b7".equals(cellInHex))
        {
            isSymbol = true;
            if(firstSymbolPosition ==0)
                firstSymbolPosition = (int)text.getX();
            return "&bull;&nbsp&nbsp&nbsp&nbsp";            
        }
        else if("ef82a7".equals(cellInHex))
        {
            isSymbol = true;
            if(firstSymbolPosition ==0)
                firstSymbolPosition = (int)text.getX();
            return "&#9632;&nbsp&nbsp&nbsp&nbsp";            
        }
        else if("ef82ae".equals(cellInHex))
        {
            isSymbol = true;
            if(firstSymbolPosition ==0)
                firstSymbolPosition = (int)text.getX();
            return "&rarr;&nbsp&nbsp&nbsp&nbsp";            
        }
        else if("ef83bc".equals(cellInHex))
        {
            isSymbol = true;
            if(firstSymbolPosition ==0)
                firstSymbolPosition = (int)text.getX();
            return "&#8730;&nbsp&nbsp&nbsp&nbsp";           
        }
        else if("ef8398".equals(cellInHex))
        {
            isSymbol = true;
            if(firstSymbolPosition ==0)
                firstSymbolPosition = (int)text.getX();
            return "&#f0d8;&nbsp&nbsp&nbsp&nbsp";            
        }
        else if("ef81b6".equals(cellInHex))
        {
            isSymbol = true;
            if(firstSymbolPosition ==0)
                firstSymbolPosition = (int)text.getX();
            return "&#f076;&nbsp&nbsp&nbsp&nbsp";          
        }
        else
        {
            isSymbol = false;
            return text.getCharacter().replace("<", "&lt;").replace(">", "&gt;");            
        }
    }
    
    public int alignmentCheck(List<Integer> firstCharOfLineStartsAt1, List<Integer> lastCharOfLineEndsAt1){
        try
        {
            int firstCharPosition = firstCharOfLineStartsAt1.get(0);
            int lastCharPos = lastCharOfLineEndsAt1.get(0);
//            System.out.println("First Charpos [0] :"+firstCharPosition+"Last Charpos [0] :"+lastCharPos);
            boolean leftAlign=true, rightAlign = true;
            for(int i = 1;i<firstCharOfLineStartsAt1.size()-1;i++)
            {
//                System.out.println("The point from alignmentCheck Methos firstpos:["+i+"]"+firstCharOfLineStartsAt1.get(i)+", lastpos:["+i+"]"+lastCharOfLineEndsAt1.get(i));
                if(Math.abs(firstCharPosition-firstCharOfLineStartsAt1.get(i))<5 && leftAlign)
                    leftAlign = true;
                else
                    leftAlign = false;

                if(Math.abs(lastCharPos - lastCharOfLineEndsAt1.get(i))<5 && rightAlign)
                    rightAlign = true;
                else
                    rightAlign = false;
            }
            if(leftAlign && rightAlign)
            {
//                System.out.println("2: Justify");
                return 2;
            }
            else if(leftAlign)
            {
//                System.out.println("0: Left");
                return 0;
            }
            else if(rightAlign)
            {
//                System.out.println("1: right");
                return 1;
            }
        }
        catch(ArrayIndexOutOfBoundsException aiobe)
        {
            System.err.println(aiobe);
        }
//       System.out.println("3: center");
        return 3;
    }

}