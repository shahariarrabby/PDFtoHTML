/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pdfreader;



import PDF.exceptions.CryptographyException;
import PDF.exceptions.InvalidPasswordException;
import PDF.pdmodel.PDDocument;
import PDF.pdmodel.PDPage;
import PDF.util.PDFTextStripperByArea;
import PDF.util.TextPosition;
import java.awt.Rectangle;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * This is an example on how to extract text from a specific area on the PDF document.
 *
 * Usage: java org.apache.pdfbox.examples.util.extractTextByArea &lt;input-pdf&gt;
 *
 * @author <a href="mailto:ben@benlitchfield.com">Ben Litchfield</a>
 * @version $Revision: 1.2 $
 */

public class ExtractTextByAreaSinglePixel
{
    public ExtractTextByAreaSinglePixel(){
    }
    
    Rectangle rec;    
    String filenamee;
    int pageNumberr;        
    int[] rowStartAt = new int[75];
    int textCount = 0;
    int numberofColumns = 0;
    int[][] trackOfTableslessLine = new int[165][900];  //Now changing temporarily int flag[][] = new int[85][125];     to the current
    PropertyControl pc = new PropertyControl();
//    List<List<TextPosition>> allRowsAsList = new ArrayList<>();
//    String[] allRowsAsString = new String[165];
    
    public int returnNumberofRows(){
        return textCount;
    }
    
    public int returnNumberofColumns(){
        return numberofColumns;
    }
    
    public int[] getPointOfRowStart(){
        return rowStartAt;
    }
    
    public String extractRegionText(String filename, Rectangle rect, int pageNumber, int forTableExtract) throws IOException, CryptographyException{
        PDDocument document = null;
        try
        {
            document = PDDocument.load( filename );
            if( document.isEncrypted() )
            {
                try
                {
                    document.decrypt( "" );
                }
                catch( InvalidPasswordException e )
                {
                    System.err.println( "Error: Document is encrypted with a password." );
                    System.exit( 1 );
                }
            }
            PDFTextStripperByArea stripper = new PDFTextStripperByArea(1);
            stripper.setSortByPosition( true );
            stripper.addRegion( "class1", rect );
            List allPages = document.getDocumentCatalog().getAllPages();
            PDPage firstPage = (PDPage)allPages.get( pageNumber );
            stripper.extractRegions( firstPage );
//            System.out.println( "Text in the area:" + rect );
            String region = stripper.getTextForRegion( "class1" );
            Vector TextinArea1 = stripper.regionCharacterList.get("class1");
            List<TextPosition> li = (List<TextPosition>) TextinArea1.get(0);
            return region;
            //System.out.println("Length of String region :"+region.length()+" Size of TextinArea :"+TextinArea.size());
//            for(int start = 0;start<region.length();start++)
//            {
////                System.out.println("This is the Font Type and Hex of "+region.charAt(start)+": " +" , "+HexStringConverter.getHexStringConverterInstance().stringToHex(region.substring(start, start+1)));
//                System.out.print(HexStringConverter.getHexStringConverterInstance().stringToHex(region.substring(start, start+1))+",");
//            }
            
//            System.out.println("Wowwwwwwwwwwwwwwwwwwwwwwwwwwww");
//            for (Iterator<TextPosition> it = TextinArea.iterator(); it.hasNext();) 
//            {
//                TextPosition text = it.next();
//                int marginLeft = (int)(text.getXDirAdj());
//                int fontSizePx = Math.round(text.getFontSizeInPt()/72*72);
//                int marginTop = (int)(text.getYDirAdj()-fontSizePx);
//                ii++;
//                System.out.println("This is the Hex of "+text.getCharacter()+"  :"+HexStringConverter.getHexStringConverterInstance().stringToHex(text.getCharacter().toString()));
////                System.out.print("Font"+text.getFont().getType());
////                System.out.print("; FontWeight :"+text.getFont().getFontDescriptor().getFontWeight());
////                System.out.print("; Italic :"+text.getFont().getFontDescriptor().isItalic());
////                System.out.println("; Character at "+ii+": "+text.getCharacter()+"; X position: "+text.getX()+"; Margin Left :"+marginLeft+"; Y position: "+text.getY()+"; Margin Top :"+marginTop+"; Width :"+text.getWidth()+"; Height :"+text.getHeight());
//            }
//            System.out.println("End of Wowwwwwwwwwwwwwwwwwwwwwwwwwwww");
        }
        finally
        {
            if( document != null )
            {
                document.close();
            }
        }
    }
        
    public List<TextPosition> extractRegionTextAllOnce(String filename, Rectangle rect, int pageNumber, int forTableExtract) throws IOException, CryptographyException{
        PDDocument document = null;
        List<TextPosition> TextinArea = new ArrayList<TextPosition>();
        try
        {
            document = PDDocument.load( filename );
            if( document.isEncrypted() )
            {
                try
                {
                    document.decrypt( "" );
                }
                catch( InvalidPasswordException e )
                {
                    System.err.println( "Error: Document is encrypted with a password." );
                    System.exit( 1 );
                }
            }
            PDFTextStripperByArea stripper = new PDFTextStripperByArea();
            stripper.setSortByPosition( true );
            stripper.addRegion( "class1", rect );
            List allPages = document.getDocumentCatalog().getAllPages();
            PDPage firstPage = (PDPage)allPages.get( pageNumber );
            stripper.extractRegions( firstPage );
//            System.out.println( "Text in the area:" + rect );
            Vector TextinArea1 = stripper.regionCharacterList.get("class1");
//            String region = stripper.getTextForRegion( "class1" );
//            System.out.println( region );
            TextinArea = (List<TextPosition>) TextinArea1.get(0);
//            int ii =0;
//            System.out.println("Length of String region :"+region.length()+" Size of TextinArea :"+TextinArea.size());
//            for(int start = 0;start<region.length();start++)
//            {
////                System.out.println("This is the Font Type and Hex of "+region.charAt(start)+": " +" , "+HexStringConverter.getHexStringConverterInstance().stringToHex(region.substring(start, start+1)));
//                System.out.print(HexStringConverter.getHexStringConverterInstance().stringToHex(region.substring(start, start+1))+",");
//            }
//            for (Iterator<TextPosition> it = TextinArea.iterator(); it.hasNext();) 
//            {
//                TextPosition text = it.next();
//                int marginLeft = (int)(text.getXDirAdj());
//                int fontSizePx = Math.round(text.getFontSizeInPt()/72*72);
//                int marginTop = (int)(text.getYDirAdj()-fontSizePx);
//                ii++;
////                System.out.println("This is the Hex of "+text.getCharacter()+"  :"+HexStringConverter.getHexStringConverterInstance().stringToHex(text.getCharacter().toString()));
////                System.out.print("Font"+text.getFont().getType());
////                System.out.print("; FontWeight :"+text.getFont().getFontDescriptor().getFontWeight());
////                System.out.print("; Italic :"+text.getFont().getFontDescriptor().isItalic());
////                System.out.println("; Character at "+ii+": "+text.getCharacter()+"; X position: "+text.getX()+"; Margin Left :"+marginLeft+"; Y position: "+text.getY()+"; Margin Top :"+marginTop+"; Width :"+text.getWidth()+"; Height :"+text.getHeight());
//            }
        }
        finally
        {
            if( document != null )
            {
                document.close();
            }
        }
        return TextinArea;
    }
        
//    public List<List<TextPosition>> getAllRowsAsList(){
//        return allRowsAsList;
//    }
//    
//    public String[] getAllRowsAsString(){
//        return allRowsAsString;
//    }
    
    public int[] extractTextByArea(String filename, Rectangle rect, int pageNumber, int forTableExtract) throws IOException, CryptographyException{
        filenamee = filename;
        rec = rect;
        pageNumberr = pageNumber;
        int dividedRegionWidth = 1;
        int NumberOftolerableLine = pc.getNumberOfTollerableLineInTable();
        PDDocument document = null;
        try
        {
            document = PDDocument.load( filename );
            if( document.isEncrypted() )
            {
                try
                {
                    document.decrypt( "" );
                }
                catch( InvalidPasswordException e )
                {
                    System.err.println( "Error: Document is encrypted with a password." );
                    System.exit( 1 );
                }
            }
            PDFTextStripperByArea stripper = new PDFTextStripperByArea();
            stripper.setSortByPosition( true );
            stripper.addRegion( "class1", rect );
            List allPages = document.getDocumentCatalog().getAllPages();
            PDPage firstPage = (PDPage)allPages.get( pageNumber );
            stripper.extractRegions( firstPage );
//            System.out.println( "Text in the area:" + rect );
            Vector TextinArea1 = stripper.regionCharacterList.get("class1");
            List<TextPosition> li = (List<TextPosition>) TextinArea1.get(0);
            int yy= 0; 
            int columnIncrement=0;
            int rowIncrement=0; 
            int flag[][] = new int[165][900];        //Now changing temporarily int flag[][] = new int[165][125];     to the current
            String character[][] = new String[165][900]; //Now changing temporarily int flag[][] = new int[165][125];     to the current
//            List<TextPosition> tempList = new ArrayList<>();
//            String tempString = " ";
            long before = System.currentTimeMillis();
            
            //<editor-fold defaultstate="collapsed" desc="comment">
            /*
             * for(TextPosition text:li)
             * {
             * System.out.println("Chars is: "+text.getCharacter()+"; Width: "+text.getWidth()+"; Y Position:"+text.getY()+"; Height: "+text.getHeight());
             * //                int yPositionOf_text = (int) Math.ceil(text.getY());
             * int yPositionOf_text = (int) Math.ceil(text.getY());
             * int findNewLine1 = (int) Math.ceil(yy);
             * int yDifferenceBetweenPrevLine = (int) Math.abs(yPositionOf_text-findNewLine1);
             * if(yPositionOf_text!=findNewLine1 && yDifferenceBetweenPrevLine>=7)
             * {
             * System.out.println("New Line Detected for the height difference 7.");
             * rowIncrement++;
             * columnIncrement = 0;
             * rowStartAt[textCount] = (int) Math.ceil(text.getY());
             * yy=text.getY();
             * textCount++;
             * }
             * int xcoordinate = (int)text.getX()/dividedRegionWidth;
             * int ycoordinate = (int)text.getY()/10+1;
             * if(!text.getCharacter().equalsIgnoreCase(" "))
             * {
             * //                    flag[ycoordinate][xcoordinate] = 1;
             * //                    character[ycoordinate][xcoordinate] = text.getCharacter();
             * //                    System.out.println("[R,C]"+"["+ycoordinate+"]["+xcoordinate+"]");
             * int incre=0;
             * for(int i =0;i<Math.ceil( text.getWidth());i+=dividedRegionWidth)
             * {
             * flag[ycoordinate][xcoordinate+incre] = 1;
             * character[ycoordinate][xcoordinate+incre] = text.getCharacter();
             * incre++;
             * //                        System.out.println("[R,C]"+"["+ycoordinate+"]["+xcoordinate+1+"]");
             * }
             * }
             * columnIncrement++;
             * }
             */
            ///////////////////Extra for test
            //</editor-fold>
            int[] getColumnsAt = new int[900]; //Now changing temporarily int flag[][] = new int[125];     to the current
            for(int liIncrement = 0; liIncrement<li.size();liIncrement++)
            {
                TextPosition text = li.get(liIncrement);
//                tempList.add(text);
//                tempString = tempString.concat(text.getCharacter());
                int yPositionOf_text = (int) Math.ceil(text.getY());
                int yDifferenceBetweenPrevLine = (int) Math.abs(yPositionOf_text - yy);
                if(textCount == 0 && yPositionOf_text!=yy)
                {
//                    allRowsAsList.add(rowIncrement,tempList);
//                    allRowsAsString[rowIncrement] = tempString;
                    rowIncrement++;
                    rowStartAt[textCount] = (int)getTheLowestYPositionOfLine(li, liIncrement)+2;
                    yy = (int)getTheLowestYPositionOfLine(li, liIncrement);                  
                    textCount++;
//                    tempList = new ArrayList<>();
//                    tempString = " ";
                }
                if(yPositionOf_text!=yy && yDifferenceBetweenPrevLine>=9)
                { 
//                    allRowsAsList.add(rowIncrement,tempList);
//                    allRowsAsString[rowIncrement] = tempString;
                    rowIncrement++;
                    columnIncrement = 0;
                    rowStartAt[textCount] = (int)getTheLowestYPositionOfLine(li, liIncrement)+2;
                    yy = (int)getTheLowestYPositionOfLine(li, liIncrement);                  
                    textCount++;
//                    tempList = new ArrayList<>();
//                    tempString = " ";
                }
                int xcoordinate = (int)text.getX()/dividedRegionWidth;
                int ycoordinate = (int)text.getY()/10+1;
                if(!text.getCharacter().equalsIgnoreCase(" "))
                {
//                    flag[ycoordinate][xcoordinate] = 1;
//                    character[ycoordinate][xcoordinate] = text.getCharacter();
//                    System.out.println("[R,C]"+"["+ycoordinate+"]["+xcoordinate+"]");
                    int incre=0;
                    for(int i =0;i<Math.ceil( text.getWidth());i+=dividedRegionWidth)
                    {
                        flag[ycoordinate][xcoordinate+incre] = 1;
                        character[ycoordinate][xcoordinate+incre] = text.getCharacter();
                        incre++;
//                        System.out.println("[R,C]"+"["+ycoordinate+"]["+xcoordinate+1+"]");
                    }                    
                }
                if("$".equals(text.getCharacter()))
                {
                    int xPosOfDollar = (int) (text.getX()-1);
                    int endXPosOfDollar = (int) (text.getX() + text.getWidth() + 2);
                    getColumnsAt[xPosOfDollar] = 1;
                    getColumnsAt[endXPosOfDollar] = 1;
                }
                columnIncrement++;
            }
            //////////////////////////
//            int[] getColumnsAt = new int[900]; //Now changing temporarily int flag[][] = new int[125];     to the current
            int[] getColumnsAtUnderTollerenceLimit = new int[900]; //Now changing temporarily int flag[][] = new int[125];     to the current
            for(int i=0;i<900;i++)  //Now changing 125  to the current // This (i) is Column Indicator
            {
                int check=0;
                int count = 0;
                for(int j = 0; j<165;j++)   //Now changing 85 to the current // This (j) is Row Indicator
                {
                    if(flag[j][i]==1)
                    {
                        check = 1;                        
                        count++;
                        trackOfTableslessLine[j][i] = j;
                    }                     
                }
                if(check == 0)
                    getColumnsAt[i] = 1;
                else if(count<=Math.ceil((rowIncrement*NumberOftolerableLine)/100))
                {
                    getColumnsAt[i] = 1;
                    getColumnsAtUnderTollerenceLimit[i] = 1;
                }
            }
            //<editor-fold defaultstate="collapsed" desc="comment">
//            System.out.println("This is FLAG array:");
//            for(int i=0;i<165;i++)
//            {
//                for(int j = 0; j<610;j++)
//                {
//                    System.out.print(flag[i][j]);
//                }
//                System.out.println();
//            }
//            System.out.println("This is character array: ");
//            for(int i=0;i<165;i++)
//            {
//                for(int j = 0; j<165;j++)
//                {
//                    System.out.print(character[i][j]);
//                }
//                System.out.println();
//            }
//            System.out.println("getColumnsAt Array:");
//            for(int i =0;i<900;i++)
//            {
//                System.out.print(getColumnsAt[i]);
//            }
//            System.out.print("getColumnsAtUnderTollerenceLimit Array:");
//            for(int i =0;i<900;i++)
//            {
//                System.out.print(getColumnsAtUnderTollerenceLimit[i]);
//            }
            //</editor-fold>
            int[] columnsAt =  new int[75];
            int lastColumn = (rect.width+rect.x)/dividedRegionWidth;
            
            for(int countcolumn = 0;countcolumn<lastColumn;countcolumn++)  //Now changing temporarily countcolumn<125 to the current
            {
                if(getColumnsAtUnderTollerenceLimit[countcolumn]==0 && getColumnsAtUnderTollerenceLimit[countcolumn+1]==1)
                {
                    getColumnsAt[countcolumn]=1; 
                }
                else if(getColumnsAtUnderTollerenceLimit[countcolumn]==1 && getColumnsAtUnderTollerenceLimit[countcolumn+1]==1)
                {
                    getColumnsAtUnderTollerenceLimit[countcolumn]=0; 
                }
                else if(getColumnsAtUnderTollerenceLimit[countcolumn]==1)
                {
                    getColumnsAt[countcolumn+1] = 0;
//                        System.out.println("Draw Columns at "+countcolumn*5);
                }
            }
            
            for(int countcolumn = 0;countcolumn<lastColumn;countcolumn++)  //Now changing temporarily countcolumn<125 to the current
            {
                if(countcolumn<lastColumn-1)  //Now changing temporarily countcolumn<124 to the current
                {
                    if(getColumnsAt[countcolumn]==0 && getColumnsAt[countcolumn+1]==1)
                    {
//                        getColumnsAt[countcolumn]=1;
                        columnsAt[numberofColumns] = countcolumn;
                        numberofColumns++;
                    }
                    else if(getColumnsAt[countcolumn]==1 && getColumnsAt[countcolumn+1]==1)
                    {
                        getColumnsAt[countcolumn]=0; 
                    }
                    else if(getColumnsAt[countcolumn]==1)
                    {
                        columnsAt[numberofColumns] = countcolumn;
                        numberofColumns++;
//                        System.out.println("Draw Columns at "+countcolumn*5);
                    }
                }
                if(countcolumn == (lastColumn-1)) //Now changing temporarily countcolumn == 124 to the current
                {
                    columnsAt[numberofColumns] = countcolumn;
                    numberofColumns++;
//                    System.out.println("Draw Columns at "+countcolumn*5);
                }
                if(numberofColumns>1 && (columnsAt[numberofColumns-1]-columnsAt[numberofColumns-2])<6)
                {
                    numberofColumns--;
                }
                    
            }
            long after = System.currentTimeMillis();  
//            System.out.println("Execution Time to find column: " + (after - before) + "ms");
            return columnsAt;
        }
        finally
        {
            if( document != null )
            {
                document.close();
            }
        }        
    }
    
    public float getTheLowestYPositionOfLine(List<TextPosition> li,int i){
        float findDiff = 0;
        float lowestY = 0;
        while(findDiff<4 && (i+1)<li.size())
        {
            lowestY = li.get(i).getY();
            findDiff = Math.abs(li.get(i+1).getY() - li.get(i).getY());
            i++; 
        }
        return lowestY;
    }
    
    public static void main( String[] args ) throws Exception{
        String arg = "D:/cv.pdf";
        PDDocument document = null;
        try
        {
            document = PDDocument.load( arg );
            if( document.isEncrypted() )
            {
                try
                {
                    document.decrypt( "" );
                }
                catch( InvalidPasswordException e )
                {
                    System.err.println( "Error: Document is encrypted with a password." );
                    System.exit( 1 );
                }
            }
            PDFTextStripperByArea stripper = new PDFTextStripperByArea();
            stripper.setSortByPosition( true );
            Rectangle rect = new Rectangle( 100, 350, 275, 60 );
            stripper.addRegion( "class1", rect );
            List allPages = document.getDocumentCatalog().getAllPages();
            PDPage firstPage = (PDPage)allPages.get( 1 );
            stripper.extractRegions( firstPage );
        }
        finally
        {
            if( document != null )
            {
                document.close();
            }
        }
    }

    private static void usage(){
        System.err.println( "Usage: java org.apache.pdfbox.examples.util.ExtractTextByArea <input-pdf>" );
    }    
}