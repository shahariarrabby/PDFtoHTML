/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pdfreader;

import PDF.exceptions.CryptographyException;
import PDF.pdmodel.PDDocument;
import PDF.pdmodel.PDPage;
import java.awt.Rectangle;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Zia
 */
public class RegionSetter {
    List<TaggedRegion> listOfSelectedRegionInRectangle;
    String pathOfPdfFile;
    HtmlFileGen htmlFileGen;
    
    public RegionSetter(String filepath,String outputFileName)
    {
        pathOfPdfFile = filepath;
        listOfSelectedRegionInRectangle = new ArrayList<TaggedRegion>();
//        try {
//            htmlFileGen = new HtmlFileGen(pathOfPdfFile);
//        } catch (IOException ex) {
//            Logger.getLogger(RegionSetter.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }
    
    public void setTaggedRegionAtList(int pageNumber,Rectangle rect,String tag){
        
        TaggedRegion taggedRegion = new TaggedRegion(pathOfPdfFile);
        taggedRegion.setTaggedRegion(pageNumber, rect, tag);
        listOfSelectedRegionInRectangle.add(taggedRegion);
    }
    public StringBuffer getHtmlContent(int pageNumber, Rectangle rec, String type)
    {
        StringBuffer htmlContent = null;
        if("table".equals(type))
        {            
            try {
                htmlContent = htmlFileGen.getTableSinglePixel(rec, pageNumber);
            } catch (CryptographyException ex) {
                Logger.getLogger(RegionSetter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return htmlContent;
    }
    
//    public void extractAllRegions(String outputFileName)
//    {
//        List<PDPage> pages;
//        try {
//            PDDocument document = PDDocument.load(pathOfPdfFile);
//            pages = document.getDocumentCatalog().getAllPages();
//            htmlFileGen = new HtmlFileGen(pathOfPdfFile);
//            try {
//                StringBuffer sbb = htmlFileGen.convertPdfToHtml(listOfSelectedRegionInRectangle,pages);
//                BufferedWriter htmlFile = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFileName),"UTF8"));
//                htmlFile.write(sbb.toString());
//                htmlFile.close();
////                System.out.println(sbb.toString());
//            } catch (Exception ex) {
//                Logger.getLogger(RegionSetter.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        } catch (IOException ex) {
//            Logger.getLogger(RegionSetter.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }    
    
    
}
