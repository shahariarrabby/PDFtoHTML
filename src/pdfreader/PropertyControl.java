/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pdfreader;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author Zia
 */
public class PropertyControl 
{     
    Properties props = new Properties();
    int displacedLineTolerancePercent = 10;
    int pixelDifference = 2;
    int NumberOfTollerableLineInTable = 10;
    
    
    public PropertyControl() 
    {
        try{
            props.load(PropertyControl.class.getResourceAsStream("settings.properties"));
        }
        
        catch(IOException ex){
            
        }
        
        displacedLineTolerancePercent = Integer.parseInt(props.getProperty("displacedLineTolerancePercent"));
        pixelDifference = Integer.parseInt(props.getProperty("pixelDifference"));
        NumberOfTollerableLineInTable = Integer.parseInt(props.getProperty("NumberOfTollerableLineInTable"));
    }
    
    public int getdisplacedLineTolerancePercent()
     {
        return displacedLineTolerancePercent;
     }
    public int getpixelDifference()
     {
        return pixelDifference;
     }
    
    public int getNumberOfTollerableLineInTable()
     {
        return NumberOfTollerableLineInTable;
     }
}
