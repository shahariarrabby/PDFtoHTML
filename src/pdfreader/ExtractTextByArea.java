/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pdfreader;



import PDF.exceptions.CryptographyException;
import PDF.exceptions.InvalidPasswordException;
import PDF.pdmodel.PDDocument;
import PDF.pdmodel.PDPage;
import PDF.util.PDFTextStripperByArea;
import PDF.util.TextPosition;
import java.awt.Rectangle;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * This is an example on how to extract text from a specific area on the PDF document.
 *
 * Usage: java org.apache.pdfbox.examples.util.extractTextByArea &lt;input-pdf&gt;
 *
 * @author <a href="mailto:ben@benlitchfield.com">Ben Litchfield</a>
 * @version $Revision: 1.2 $
 */
public class ExtractTextByArea
{
    public ExtractTextByArea()
    {
        //utility class and should not be constructed.
    }


    /**
     * This will print the documents text in a certain area.
     *
     * @param args The command line arguments.
     *
     * @throws Exception If there is an error parsing the document.
     */
    
  
    
//    int numberofColumns;
    
    Rectangle rec;    
    String filenamee;
    int pageNumberr;
    public void getTable() throws IOException, CryptographyException
    {
//        int forTableExtract = 1;
//        Rectangle[] ColumnWiseRect = null;
//        for(int i = 0;i<NumberofColumns-1;i++)
//        {
//            if(i ==0)
//            {
//                ColumnWiseRect[i] = new Rectangle(rec.x,rec.y,columnsAt[i]*5,rec.height);
//                extractTextByArea(filenamee,ColumnWiseRect[i],pageNumberr,forTableExtract);
//            }
//            else
//            {
//                ColumnWiseRect[i] = new Rectangle(columnsAt[i-1]*5,rec.y,columnsAt[i]*5,rec.height);
//                extractTextByArea(filenamee,ColumnWiseRect[i],pageNumberr,forTableExtract);
//            }
//        }
                        
    }
    int[] rowStartAt = new int[75];
    int textCount = 0;
    int numberofColumns = 0;
    
    public int returnNumberofRows()
    {
        return textCount;
    }
    
    public int returnNumberofColumns()
    {
        return numberofColumns;
    }
    
    public int[] getPointOfRowStart()
    {
//        for(int i = 0;i<rowStartAt.length;i++)
//        {
//            System.out.println("Row Start at:"+rowStartAt[i]);
//        }
        return rowStartAt;
    }
    
    int[][] trackOfTableslessLine = new int[165][165];  //Now changing temporarily int flag[][] = new int[85][125];     to the current
    public void extractRegionText(String filename, Rectangle rect, int pageNumber, int forTableExtract) throws IOException, CryptographyException
    {
        PDDocument document = null;
        try
        {
            document = PDDocument.load( filename );
            if( document.isEncrypted() )
            {
                try
                {
                    document.decrypt( "" );
                }
                catch( InvalidPasswordException e )
                {
                    System.err.println( "Error: Document is encrypted with a password." );
                    System.exit( 1 );
                }
            }
            PDFTextStripperByArea stripper = new PDFTextStripperByArea(1);
            stripper.setSortByPosition( true );
            stripper.addRegion( "class1", rect );
            List allPages = document.getDocumentCatalog().getAllPages();
            PDPage firstPage = (PDPage)allPages.get( pageNumber );
            stripper.extractRegions( firstPage );
//            System.out.println( "Text in the area:" + rect );
            String region = stripper.getTextForRegion( "class1" );
            Vector TextinArea1 = stripper.regionCharacterList.get("class1");
//            for(int i = 0; i<TextinArea1.size(); i++)
//            {
//                List li = (ArrayList) TextinArea1.get(i);
//                for(int j = 0; j< li.size();j++)
//                    System.out.println("Here it is!--------"+li.get(j));
//            }
//            System.out.println( region );
            List<TextPosition> TextinArea = stripper.getAllSelectedText();
            int ii =0;
            //System.out.println("Length of String region :"+region.length()+" Size of TextinArea :"+TextinArea.size());
//            for(int start = 0;start<region.length();start++)
//            {
////                System.out.println("This is the Font Type and Hex of "+region.charAt(start)+": " +" , "+HexStringConverter.getHexStringConverterInstance().stringToHex(region.substring(start, start+1)));
//                System.out.print(HexStringConverter.getHexStringConverterInstance().stringToHex(region.substring(start, start+1))+",");
//            }
            
//            System.out.println("Wowwwwwwwwwwwwwwwwwwwwwwwwwwww");
//            for (Iterator<TextPosition> it = TextinArea.iterator(); it.hasNext();) 
//            {
//                TextPosition text = it.next();
//                int marginLeft = (int)(text.getXDirAdj());
//                int fontSizePx = Math.round(text.getFontSizeInPt()/72*72);
//                int marginTop = (int)(text.getYDirAdj()-fontSizePx);
//                ii++;
//                System.out.println("This is the Hex of "+text.getCharacter()+"  :"+HexStringConverter.getHexStringConverterInstance().stringToHex(text.getCharacter().toString()));
////                System.out.print("Font"+text.getFont().getType());
////                System.out.print("; FontWeight :"+text.getFont().getFontDescriptor().getFontWeight());
////                System.out.print("; Italic :"+text.getFont().getFontDescriptor().isItalic());
////                System.out.println("; Character at "+ii+": "+text.getCharacter()+"; X position: "+text.getX()+"; Margin Left :"+marginLeft+"; Y position: "+text.getY()+"; Margin Top :"+marginTop+"; Width :"+text.getWidth()+"; Height :"+text.getHeight());
//            }
//            System.out.println("End of Wowwwwwwwwwwwwwwwwwwwwwwwwwwww");
        }
        finally
        {
            if( document != null )
            {
                document.close();
            }
        }
    }
    
    
    public List<TextPosition> extractRegionTextAllOnce(String filename, Rectangle rect, int pageNumber, int forTableExtract) throws IOException, CryptographyException
    {
        PDDocument document = null;
        List<TextPosition> TextinArea = new ArrayList<TextPosition>();
        try
        {
            document = PDDocument.load( filename );
            if( document.isEncrypted() )
            {
                try
                {
                    document.decrypt( "" );
                }
                catch( InvalidPasswordException e )
                {
                    System.err.println( "Error: Document is encrypted with a password." );
                    System.exit( 1 );
                }
            }
            PDFTextStripperByArea stripper = new PDFTextStripperByArea();
            stripper.setSortByPosition( true );
            stripper.addRegion( "class1", rect );
            List allPages = document.getDocumentCatalog().getAllPages();
            PDPage firstPage = (PDPage)allPages.get( pageNumber );
            stripper.extractRegions( firstPage );
//            System.out.println( "Text in the area:" + rect );
            Vector TextinArea1 = stripper.regionCharacterList.get("class1");
//            String region = stripper.getTextForRegion( "class1" );
//            System.out.println( region );
            TextinArea = (List<TextPosition>) TextinArea1.get(0);
//            int ii =0;
//            System.out.println("Length of String region :"+region.length()+" Size of TextinArea :"+TextinArea.size());
//            for(int start = 0;start<region.length();start++)
//            {
////                System.out.println("This is the Font Type and Hex of "+region.charAt(start)+": " +" , "+HexStringConverter.getHexStringConverterInstance().stringToHex(region.substring(start, start+1)));
//                System.out.print(HexStringConverter.getHexStringConverterInstance().stringToHex(region.substring(start, start+1))+",");
//            }
//            for (Iterator<TextPosition> it = TextinArea.iterator(); it.hasNext();) 
//            {
//                TextPosition text = it.next();
//                int marginLeft = (int)(text.getXDirAdj());
//                int fontSizePx = Math.round(text.getFontSizeInPt()/72*72);
//                int marginTop = (int)(text.getYDirAdj()-fontSizePx);
//                ii++;
////                System.out.println("This is the Hex of "+text.getCharacter()+"  :"+HexStringConverter.getHexStringConverterInstance().stringToHex(text.getCharacter().toString()));
////                System.out.print("Font"+text.getFont().getType());
////                System.out.print("; FontWeight :"+text.getFont().getFontDescriptor().getFontWeight());
////                System.out.print("; Italic :"+text.getFont().getFontDescriptor().isItalic());
////                System.out.println("; Character at "+ii+": "+text.getCharacter()+"; X position: "+text.getX()+"; Margin Left :"+marginLeft+"; Y position: "+text.getY()+"; Margin Top :"+marginTop+"; Width :"+text.getWidth()+"; Height :"+text.getHeight());
//            }
        }
        finally
        {
            if( document != null )
            {
                document.close();
            }
        }
        return TextinArea;
    }
          
    
//    File fiile = new File("D:/ziaa.txt");
//    BufferedWriter outputt = new BufferedWriter(new FileWriter(fiile));      
//    BufferedWriter ouutputt = new BufferedWriter(new FileWriter(fiile));
    public int[] extractTextByArea(String filename, Rectangle rect, int pageNumber, int forTableExtract) throws IOException, CryptographyException
    {
//        File fiile = new File("G:/PDF2HTMLTest/ziaa.txt");
//        BufferedWriter outputt = new BufferedWriter(new FileWriter(fiile)); 
        filenamee = filename;
        rec = rect;
        pageNumberr = pageNumber;
        PropertyControl pc = new PropertyControl();
        int NumberOftolerableLine = pc.getNumberOfTollerableLineInTable();
        PDDocument document = null;
        try
        {
            document = PDDocument.load( filename );
            if( document.isEncrypted() )
            {
                try
                {
                    document.decrypt( "" );
                }
                catch( InvalidPasswordException e )
                {
                    System.err.println( "Error: Document is encrypted with a password." );
                    System.exit( 1 );
                }
            }
            PDFTextStripperByArea stripper = new PDFTextStripperByArea();
            stripper.setSortByPosition( true );
            stripper.addRegion( "class1", rect );
            List allPages = document.getDocumentCatalog().getAllPages();
            PDPage firstPage = (PDPage)allPages.get( pageNumber );
            stripper.extractRegions( firstPage );
//            System.out.println( "Text in the area:" + rect );
            Vector TextinArea1 = stripper.regionCharacterList.get("class1");
            List<TextPosition> li = (List<TextPosition>) TextinArea1.get(0);
            List<TextPosition> TextinArea = stripper.getAllSelectedText();
            float yy=0; 
            int columnIncrement=0;
            int rowIncrement=0; 
            int flag[][] = new int[165][165];        //Now changing temporarily int flag[][] = new int[165][125];     to the current
            String character[][] = new String[165][165]; //Now changing temporarily int flag[][] = new int[165][125];     to the current
            for(TextPosition text:li)
            {
//                System.out.println("Chars are: "+text.getCharacter());
//                int findNewLine = (int) Math.ceil(text.getY());                
                int findNewLine = (int) Math.ceil(text.getY());
                int findNewLine1 = (int) Math.ceil(yy);
                int findNewLine2 = (int) Math.abs(findNewLine-findNewLine1);
                if(findNewLine!=findNewLine1 && findNewLine2>=7)
                { 
                    rowIncrement++;
                    columnIncrement = 0;
                    rowStartAt[textCount] = (int) Math.ceil(text.getY());
                    yy=text.getY();                  
                    textCount++;
                }
                int xcoordinate = (int)text.getX()/5+1;
                int ycoordinate = (int)text.getY()/10+1;
                if(!text.getCharacter().equalsIgnoreCase(" "))
                {
                    flag[ycoordinate][xcoordinate] = 1;
                    character[ycoordinate][xcoordinate] = text.getCharacter();
//                    System.out.println("[R,C]"+"["+ycoordinate+"]["+xcoordinate+"]");
                    if(text.getWidth()>5)
                    {
                        flag[ycoordinate][xcoordinate+1] = 1;
                        character[ycoordinate][xcoordinate] = text.getCharacter();
//                        System.out.println("[R,C]"+"["+ycoordinate+"]["+xcoordinate+1+"]");
                    }                    
                }
                columnIncrement++;
            }
            long before = System.currentTimeMillis();
            int[] getColumnsAt = new int[165]; //Now changing temporarily int flag[][] = new int[125];     to the current
            for(int i=0;i<165;i++)  //Now changing 125  to the current // This (i) is Column Indicator
            {
                int check=0;
                int count = 0;
                for(int j = 0; j<165;j++)   //Now changing 85 to the current // This (j) is Row Indicator
                {
                    if(flag[j][i]==1)
                    {
                        check = 1;                        
                        count++;
                        trackOfTableslessLine[j][i] = j;
                    }                     
                }
                if(check == 0)
                    getColumnsAt[i] = 1;
                else if(count<=NumberOftolerableLine)
                {
                    getColumnsAt[i] = 1;
                }
            }
            //<editor-fold defaultstate="collapsed" desc="comment">
//            System.out.println("This is FLAG array:");
//            for(int i=0;i<165;i++)
//            {
//                for(int j = 0; j<165;j++)
//                {
//                    System.out.print(flag[i][j]);
//                }
//                System.out.println();
//            }
//            System.out.println("This is character array: ");
//            for(int i=0;i<165;i++)
//            {
//                for(int j = 0; j<165;j++)
//                {
//                    System.out.print(character[i][j]);
//                }
//                System.out.println();
//            }
            //</editor-fold>
            int[] columnsAt =  new int[20];
            int lastColumn = (rect.width+rect.x)/5;
            for(int countcolumn = 0;countcolumn<lastColumn;countcolumn++)  //Now changing temporarily countcolumn<125 to the current
            {
                if(countcolumn<164)  //Now changing temporarily countcolumn<124 to the current
                {
                    if(getColumnsAt[countcolumn]==1 && getColumnsAt[countcolumn+1]==1)
                    {
                        getColumnsAt[countcolumn]=0; 
                    }
                    else if(getColumnsAt[countcolumn]==1)
                    {
                        columnsAt[numberofColumns] = countcolumn;
                        numberofColumns++;
//                        System.out.println("Draw Columns at "+countcolumn*5);
                    }
                }
                if(countcolumn == (lastColumn-1)) //Now changing temporarily countcolumn == 124 to the current
                {
                    columnsAt[numberofColumns] = countcolumn;
                    numberofColumns++;
//                    System.out.println("Draw Columns at "+countcolumn*5);
                }
            }
//            System.out.println(stripper.getTextForRegion("class1"));
//            outputt.write(stripper.getTextForRegion( "class1" ));
//            outputt.close();
            long after = System.currentTimeMillis();  
//            System.out.println("Execution Time to find column: " + (after - before) + "ms");
            return columnsAt;
        }
        finally
        {
            if( document != null )
            {
                document.close();
            }
        }
        
    }
    
    public static void main( String[] args ) throws Exception
    {
        String arg = "D:/cv.pdf";
        PDDocument document = null;
        try
        {
            document = PDDocument.load( arg );
            if( document.isEncrypted() )
            {
                try
                {
                    document.decrypt( "" );
                }
                catch( InvalidPasswordException e )
                {
                    System.err.println( "Error: Document is encrypted with a password." );
                    System.exit( 1 );
                }
            }
            PDFTextStripperByArea stripper = new PDFTextStripperByArea();
            stripper.setSortByPosition( true );
            Rectangle rect = new Rectangle( 100, 350, 275, 60 );
            stripper.addRegion( "class1", rect );
            List allPages = document.getDocumentCatalog().getAllPages();
            PDPage firstPage = (PDPage)allPages.get( 1 );
            stripper.extractRegions( firstPage );
        }
        finally
        {
            if( document != null )
            {
                document.close();
            }
        }
    }

    /**
     * This will print the usage for this document.
     */
    private static void usage()
    {
        System.err.println( "Usage: java org.apache.pdfbox.examples.util.ExtractTextByArea <input-pdf>" );
    }    
}