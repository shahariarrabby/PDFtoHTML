/*
 * To change this template, choose Tools | Templates
 * and open the template imageNumber the editor.
 */
package pdfreader;

/**
 *
 * @author Zia
 */

import PDF.exceptions.CryptographyException;
import PDF.pdmodel.PDDocument;
import PDF.pdmodel.PDPage;
import PDF.util.ImageIOUtil;
import PDF.util.PDFTextStripper;
import PDF.util.TextPosition;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HtmlFileGen extends PDFTextStripper
{
    float previousAveCharWidth = -1;
    List<PDPage> pages = null;
    int spanCounter = 0;
    String pdfPath;   
    ListExtraction listt;
    List<TextPosition> TextinAr;
    int imageNum = 0;        
    String strDirectoy = null;
    String imgSavingDirectory = null;
    String imgSavingURL = null;
    

    public HtmlFileGen(String pathOfPdfFile, String imageSavingDirectory, String imageSavingURL, String projectID, String fileID) throws IOException{
        imgSavingDirectory = imageSavingDirectory.concat("p"+projectID+"_f"+fileID+"-");
        imgSavingURL = imageSavingURL.concat("p"+projectID+"_f"+fileID+"-");
        pdfPath = pathOfPdfFile;
        File f =null;
        if (pathOfPdfFile.toLowerCase().endsWith(".pdf"))
        {
            strDirectoy = pathOfPdfFile.substring(0, pathOfPdfFile.length()-4).concat("_images");
            f = new File(strDirectoy);
        }
        if(!f.exists())
        {            
            boolean success = (new File(strDirectoy)).mkdir();
        }
    }
    
    
    ExtractTextByArea extractTextByArea;
    
    public String getHtmlContent(int pageNumber, Rectangle rec, String type){
        StringBuffer htmlContent = null;
        if("table".equals(type))
        {
            try {
                htmlContent = getTableSinglePixel(rec,pageNumber);
            } catch (CryptographyException ex) {
                Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if("paragraph".equals(type))
        {
            try {
                extractTextByArea = new ExtractTextByArea();
                TextinAr = extractTextByArea.extractRegionTextAllOnce(pdfPath, rec, pageNumber, 0);
                listt = new ListExtraction(5,2);
                TextPosition text;
//                for (Iterator<TextPosition> it = TextinAr.iterator(); it.hasNext();) 
//                System.out.println("Paragraph Started :");
                for(int i = 0; i<TextinAr.size();i++)
                {
                    text = TextinAr.get(i);
//                    text = it.next();
                    listt.processTextt(text);
                }
                htmlContent = replaceAllWeiredChars(listt.getParagraph());
            } catch (IOException ex) {
                Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
            } catch (CryptographyException ex) {
                Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if("text_with_line_break".equals(type))
        {
            try {
                extractTextByArea = new ExtractTextByArea();
                TextinAr = extractTextByArea.extractRegionTextAllOnce(pdfPath, rec, pageNumber, 0);
                listt = new ListExtraction(3,2);
                TextPosition text;
                for (Iterator<TextPosition> it = TextinAr.iterator(); it.hasNext();) 
                {
                    text = it.next();
                    listt.processTextt(text);
                }
                htmlContent = replaceAllWeiredChars(listt.getParagraph());
            } catch (IOException ex) {
                Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
            } catch (CryptographyException ex) {
                Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if("list".equals(type))
        {
            try {
                htmlContent = getListSinglePixel(pdfPath,rec,pageNumber);
            } catch (CryptographyException ex) {
                Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if("image".equals(type))
        {
            htmlContent = saveImage(pageNumber,rec,imageNum);                
            imageNum++;
        }
//        String stringUsingUTF8 = null;
//        try {
//            byte[] bytesInUTF8 = htmlContent.toString().getBytes("UTF-8");
//            stringUsingUTF8 = new String(bytesInUTF8, "UTF-8");
//        } catch (UnsupportedEncodingException ex) {
//            Logger.getLogger(HtmlFileGen.class.getName()).log(Level.SEVERE, null, ex);
//        }
        return htmlContent.toString();
//        return stringUsingUTF8;
    }

    private StringBuffer saveImage(int pNumber,Rectangle rect,int imageNumber){
        try 
        {
            PDDocument doc = PDDocument.load(pdfPath);
            pages = doc.getDocumentCatalog().getAllPages();
            PDPage pageToSave = (PDPage)pages.get(pNumber);
            BufferedImage pageAsImage = pageToSave.convertToImage();
            pageAsImage = pageAsImage.getSubimage(rect.x*2, rect.y*2, rect.width*2, rect.height*2);
            String imageFilename = imgSavingDirectory;
            imageFilename += pNumber + "-" + imageNumber; 
            ImageIOUtil.writeImage(pageAsImage, "jpg", imageFilename,  BufferedImage.TYPE_USHORT_565_RGB, 300);            
            String imageURL = imgSavingURL.concat(pNumber + "-" + imageNumber);
//            System.out.println("Image Directory :"+imageFilename+"; Image URL :"+imageURL);
            String ss = "<p padding-left = \""+rect.x+"px\"><img src=\""+imageURL+".jpg\" "+ "height=\""+rect.height*2+"px\" width=\""+rect.width*2+"px\"/></p>";            
            return new StringBuffer(ss);
        }
        catch (IOException exception)
        {
        }
        return null;
    }
    
    public StringBuffer getTableSinglePixel(Rectangle rectangle,int currentPage) throws CryptographyException{           
        
        int dividedRegionWidth = 1;
        StringBuffer sb = null;
        int[] regioon = null;
        ExtractTextByAreaSinglePixel ETB = new ExtractTextByAreaSinglePixel();
        try {
            regioon = ETB.extractTextByArea(pdfPath,rectangle,currentPage,0);
        } catch (IOException ex) {
            Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
        }
        
//        long before = System.currentTimeMillis();
        int positionOfRowStart[] = ETB.getPointOfRowStart();
        int numberofRows = ETB.returnNumberofRows();
        int numberofColumns = ETB.returnNumberofColumns();
        
        List<TextPosition>[][] aCWTP;
       
        ExtractTextByColumn ETBC = new ExtractTextByColumn(pdfPath,currentPage); 
        
        Rectangle[][] ColumnWiseRect = new Rectangle[numberofRows][numberofColumns-1];
        int[][] cellSpan = new int[numberofRows][numberofColumns];     
        
        for(int row = 0; row < numberofRows ; row++)
        {
            for(int column = 0; column < numberofColumns - 1 ; column++)
            {
                if(column ==0 && row==0)
                    ColumnWiseRect[row][column] = new Rectangle(rectangle.x,rectangle.y,(regioon[column+1]*dividedRegionWidth-rectangle.x),positionOfRowStart[row]-rectangle.y);

                else if(row == 0 && column>0)
                    ColumnWiseRect[row][column] = new Rectangle(regioon[column]*dividedRegionWidth,rectangle.y,(regioon[column+1]*dividedRegionWidth-regioon[column]*dividedRegionWidth),positionOfRowStart[row]-rectangle.y);

                else if(column == 0 && row>0)
                    ColumnWiseRect[row][column] = new Rectangle(rectangle.x,positionOfRowStart[row-1],(regioon[column+1]*dividedRegionWidth-rectangle.x),(positionOfRowStart[row]-positionOfRowStart[row-1]));

                else if(column>0 && row>0)
                    ColumnWiseRect[row][column] = new Rectangle(regioon[column]*dividedRegionWidth,positionOfRowStart[row-1],(regioon[column+1]*dividedRegionWidth-regioon[column]*dividedRegionWidth),(positionOfRowStart[row]-positionOfRowStart[row-1]));
            }
        }
        try {
            ETBC.ExtractTextByArea(ColumnWiseRect,numberofRows,numberofColumns-1);
        } catch (IOException ex) {
            Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        aCWTP = ETBC.getAllCellsWithTextProperties();
        for(int rowPos=0;rowPos<numberofRows;rowPos++)
        {
            for(int columnPos = 1;columnPos<numberofColumns-1;columnPos++)
            {
                if(aCWTP[rowPos][columnPos] != null  && aCWTP[rowPos][columnPos].size() >= 1 )
                { 
                    TextPosition firstColLastChar = getLastSignificantChar(aCWTP,rowPos,columnPos-1);
                    TextPosition secColFirstChar = getFirstSignificantChar(aCWTP,rowPos,columnPos);
                    float defaultWordSpace = 5;
                    if((firstColLastChar != null && secColFirstChar != null) )
                    {
                        float gap = secColFirstChar.getX() - (firstColLastChar.getX() + firstColLastChar.getWidth());
                        if(spanCounter>0)
                            defaultWordSpace = 5f;
                        if(gap<=defaultWordSpace)
                        {                        
                            int currentSpan = cellSpan [ rowPos ] [ columnPos  - spanCounter - 1 ];
                            int executeStartPos = columnPos - currentSpan - spanCounter - 1 ;
                            for(int k = executeStartPos; k <= executeStartPos + currentSpan + spanCounter + 1; k ++)
                            {
                                cellSpan[ rowPos ] [ k ] = currentSpan + spanCounter + 1;
                            }
                        }
                    }
                    spanCounter = 0;
                }            
            }
        }
        try {
            sb = ETBC.getTableWithAllCellsSpan(pdfPath,currentPage,ColumnWiseRect,rectangle,cellSpan,numberofRows,numberofColumns);
        } catch (IOException ex) {
            Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
        }
        sb = replaceAllWeiredChars(sb);
        return sb;
    }
    
    private StringBuffer replaceAllWeiredChars(StringBuffer sb){
        String ss = sb.toString();
        ss = ss.replace("•", "&bull;").replace("®", "&#174;").replace("†", "&#8224;").replace("’", "&#8217;").replace("”", "&#8221;").replace("“", "&#8220;")
                .replace("—", "&#8212;").replace("–", "&#8211;").replace(" ", " ").replace("©", "&#169;").replace("­","&#8211;");
        StringBuffer stringBuffer = new StringBuffer(ss);
        return stringBuffer;
    }
        
    public StringBuffer getListSinglePixel(String pdfFile,Rectangle rectangle,int currentPage) throws CryptographyException{           
        
        StringBuffer sb = null;
        ExtractTextByAreaSinglePixel ETB = new ExtractTextByAreaSinglePixel();
        try {
            ETB.extractTextByArea(pdfFile,rectangle,currentPage,0);
        } catch (IOException ex) {
            Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
        }
//        long before = System.currentTimeMillis();
        int positionOfRowStart[] = ETB.getPointOfRowStart();
        int numberofRows = ETB.returnNumberofRows();
        int numberofColumns = ETB.returnNumberofColumns();

        ExtractTextByColumn ETBC = new ExtractTextByColumn();
        Rectangle[] ColumnWiseRect = new Rectangle[numberofRows];
        int[][] cellSpan = new int[numberofRows][numberofColumns];        
        
        for(int row = 0; row < numberofRows ; row++)
        {
            if(row==0)
                ColumnWiseRect[row] = new Rectangle(rectangle.x,rectangle.y,rectangle.width,positionOfRowStart[row]-rectangle.y);
            else if(row>0)
                ColumnWiseRect[row] = new Rectangle(rectangle.x,positionOfRowStart[row-1],rectangle.width,(positionOfRowStart[row]-positionOfRowStart[row-1]));
        }
        
        try {
            ETBC.ExtractTextByArea1(pdfFile,ColumnWiseRect,currentPage,numberofRows,2-1);
        } catch (IOException ex) {
            Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //        //<editor-fold defaultstate="collapsed" desc="comment">
//        aCWTP = ETBC.getAllCellsWithTextProperties();
        //        for(int rowPos=0;rowPos<numberofRows;rowPos++)
        //        {
        //            for(int columnPos = 1;columnPos<numberofColumns-1;columnPos++)
        //            {
        //                if(aCWTP[rowPos][columnPos] != null  && aCWTP[rowPos][columnPos].size() >= 1 )
        //                {
        //                    //TextPosition firstColLastChar = aCWTP[rowPos][columnPos-1].get((aCWTP[rowPos][columnPos - 1].size()-1));
        //                    //TextPosition secColFirstChar = aCWTP[rowPos][columnPos].get(0);
        //                    TextPosition firstColLastChar = getLastSignificantChar(aCWTP,rowPos,columnPos-1);
        //                    TextPosition secColFirstChar = getFirstSignificantChar(aCWTP,rowPos,columnPos);
        ////                    System.out.println("i :"+rowPos+", j :"+columnPos+", Span Counter :"+spanCounter);
        //                    float defaultWordSpace = 5;
        //                    if((firstColLastChar != null && secColFirstChar != null) )
        //                    {
        ////                        System.out.println("For GAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP");
        //                        float gap = secColFirstChar.getX() - (firstColLastChar.getX() + firstColLastChar.getWidth());
        //                        if(spanCounter>0)
        //                            defaultWordSpace = 5f;
        //                        if(gap<=defaultWordSpace)
        //                        {
        //                            int currentSpan = cellSpan [ rowPos ] [ columnPos  - spanCounter - 1 ];
        ////                            System.out.println("Current Span :"+currentSpan+" at the position: "+rowPos+" ,"+(columnPos - spanCounter - 1));
        //                            int executeStartPos = columnPos - currentSpan - spanCounter - 1 ;
        //                            for(int k = executeStartPos; k <= executeStartPos + currentSpan + spanCounter + 1; k ++)
        //                            {
        //                                cellSpan[ rowPos ] [ k ] = currentSpan + spanCounter + 1;
        ////                                System.out.println("Cell Span :"+cellSpan[rowPos][k]+" at "+rowPos+", "+k);
        //                            }
        //                        }
        //                    }
        ////                    if(spanCounter > 0 && flagForSpanCounter)
        ////                    {
        ////                        System.out.println("For Span Counterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr");
        ////                            int currentSpan = cellSpan [ rowPos ] [ columnPos - spanCounter -1];
        ////                            System.out.println("Current Span :"+currentSpan+" at the position: "+rowPos+" ,"+(columnPos - spanCounter -1));
        ////                            int executeStartPos = columnPos - currentSpan - spanCounter ;
        ////                            for(int k = executeStartPos; k <= executeStartPos + currentSpan + spanCounter + 1; k ++)
        ////                            {
        ////                                cellSpan[ rowPos ] [ k ] = currentSpan +spanCounter+ 1;
        ////                                System.out.println("Cell Span :"+cellSpan[rowPos][k]+" at "+rowPos+", "+k);
        ////                            }
        ////                            flagForSpanCounter = false;
        ////                    }
        //                    spanCounter = 0;
        //                }
        //            }
        //        }
        
        
//        long after = System.currentTimeMillis();  
//        System.out.println("Execution Time to set the text to their cell: " + (after - before) + "ms"); 
        //</editor-fold>
        
        try {
            sb = ETBC.getListWithCellSpan(pdfFile,currentPage,ColumnWiseRect,rectangle,cellSpan,numberofRows,numberofColumns);
//            sb = ETBC.getListWithAllCellSpan(pdfFile,currentPage,ColumnWiseRect,rectangle,cellSpan,numberofRows,numberofColumns);
        } catch (IOException ex) {
            Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
        }
        sb = replaceAllWeiredChars(sb);
        return sb;
    }
       
    public TextPosition getLastSignificantChar(List<TextPosition>[][] cellText, int rowPos, int columnPos){
        for (int i = columnPos; i >= 0; i--) 
        {
            if(cellText[rowPos][i].size()>0)
            {
                for (int j = cellText[rowPos][i].size() - 1; j >=0 ; j--) 
                {
                    if(!cellText[rowPos][i].get(j).getCharacter().equals(" "))
                    {
                        return cellText[rowPos][i].get(j);
                    }                
                }
            }
            spanCounter++;
        }
        return null;
    }
    
    public TextPosition getFirstSignificantChar(List<TextPosition>[][] cellText,int row,int col){
        
        for (int i = 0; i < cellText[row][col].size() ; i++) {
            if(!cellText[row][col].get(i).getCharacter().equals(" ") && !cellText[row][col].get(i).getCharacter().equals(")")){
                return cellText[row][col].get(i);
            }
        }
        return null;
    }

//    public static void main(String[] args){
//        try {
//            HtmlFileGen htmlFileGen = new HtmlFileGen("G:/From Fuad sir DOCX to PDF/form10q.pdf");
//            System.out.println(htmlFileGen.getHtmlContent(1, new Rectangle(55, 59, 493, 262), "table").toString());
////            System.out.println(htmlFileGen.getHtmlContent(1, new Rectangle(55, 59, 493, 262), "list").toString());
////            System.out.println(htmlFileGen.getHtmlContent(1, new Rectangle(55, 59, 493, 262), "text_with_line_break").toString());
////            System.out.println(htmlFileGen.getHtmlContent(2, new Rectangle(55, 59, 493, 262), "paragraph").toString());
////            System.out.println(htmlFileGen.getHtmlContent(3, new Rectangle(55, 59, 493, 262), "paragraph").toString());
////            System.out.println(htmlFileGen.getHtmlContent(0, new Rectangle(50, 366, 508, 349), "image").toString());
//        } catch (IOException ex) {
//            Logger.getLogger(RegionSetter.class.getName()).log(Level.SEVERE, null, ex);
//        }
////        <Region x="55" y="59" width="493" height="262" type="paragraph"/>
////        <Region x="50" y="366" width="508" height="349" type="paragraph"/>
////        rs.setTaggedRegionAtList(2, new Rectangle(60, 141, 496, 266), "table");        
////        rs.setTaggedRegionAtList(3, new Rectangle(60, 141, 496, 266), "table");
////        rs.setTaggedRegionAtList(4, new Rectangle(60, 141, 496, 266), "table");
////        rs.setTaggedRegionAtList(5, new Rectangle(60, 141, 496, 266), "table");
////        rs.setTaggedRegionAtList(6, new Rectangle(60, 141, 496, 266), "table");
////        rs.setTaggedRegionAtList(7, new Rectangle(60, 141, 496, 266), "table");
////        rs.setTaggedRegionAtList(8, new Rectangle(60, 141, 496, 266), "table");
//    }

}