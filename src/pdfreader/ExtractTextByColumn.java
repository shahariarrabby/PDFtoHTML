/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pdfreader;


import PDF.exceptions.CryptographyException;
import PDF.exceptions.InvalidPasswordException;
import PDF.pdmodel.PDDocument;
import PDF.pdmodel.PDPage;
import PDF.pdmodel.font.PDFont;
import PDF.pdmodel.font.PDFontDescriptor;
import PDF.pdmodel.interactive.annotation.PDBorderStyleDictionary;
import PDF.util.PDFTextStripperByArea;
import PDF.util.TextPosition;
import java.awt.Rectangle;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JFileChooser;

public class ExtractTextByColumn {
    
    PropertyControl pc = new PropertyControl();
    String[][] tableData = new String[75][50];
    List<TextPosition>[][] allCellsList = new List[100][50];
    TextPosition[][] leftLetter = new TextPosition[75][50];
    TextPosition[][] rightLetter = new TextPosition[75][50];
    int pixelDifference = pc.getpixelDifference();
    int displacedLineTolerancePercent = pc.getdisplacedLineTolerancePercent();
    JFileChooser fileDialog;
    String[] allignmentCheck;
    Rectangle[][] rectangles;
    List<TextPosition> li;
    int foundCharAt;
    boolean isSymbol;
    String regex = "[(a-zA-Z0-9]+[ ]+[.)]";
    String numberPattern = "^\\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher;
    int start;
    int end;
    PDF.pdmodel.PDDocument document = null;
    PDF.pdmodel.PDDocument document1 = null;
    int pageNumber;
    PDFTextStripperByArea stripper;
    PDFTextStripperByArea stripper1;
    PDPage firstPage;
    PDPage firstPage1;
    private int lastMarginTop = 0;
    private boolean lastIsBold = false;
    private boolean lastIsItalic = false;
    
    
    
    public ExtractTextByColumn(){
        
    }

    ExtractTextByColumn(String pdfFile, int currentPage) {
        pageNumber = currentPage;
       
        try {
            document = PDDocument.load( pdfFile );
            document1 = PDDocument.load( pdfFile );
            if( document.isEncrypted() )
            {
                try{ try { document.decrypt( "" ); } catch (CryptographyException ex) { Logger.getLogger(ExtractTextByColumn.class.getName()).log(Level.SEVERE, null, ex); } }

                    catch( InvalidPasswordException e ){System.err.println( "Error: Document is encrypted with a password." ); System.exit( 1 );}
            }
            stripper = new PDFTextStripperByArea(1);
            stripper.setSortByPosition( true );
            stripper1 = new PDFTextStripperByArea(1);
            stripper1.setSortByPosition( true );
            List allPages = document.getDocumentCatalog().getAllPages();
            firstPage = (PDPage)allPages.get( pageNumber );
            List allPages1 = document1.getDocumentCatalog().getAllPages();
            firstPage1 = (PDPage)allPages1.get( pageNumber );
            
        } catch (IOException ex) {
            Logger.getLogger(ExtractTextByColumn.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public int getLeftMostXPos(int numberofRows){
        int leftMostXPos = 1000;
        for(int j = 0;j<numberofRows;j++)
        {
            if(leftLetter[j][0] != null && leftLetter[j][0].getCharacter()!= null)
            {
                if(leftLetter[j][0].getX()<leftMostXPos)
                {
                    leftMostXPos = (int)leftLetter[j][0].getX();
                }
            }
        }
//        System.out.println("Left Most Position of First Column is: "+leftMostXPos);
        return leftMostXPos;
    }
    
    public int[] findAllignment(int numberofRows,int numberOfColumns){
        allignmentCheck = new String[numberOfColumns];        
        int[] indentation = new int[numberofRows];
//        System.out.println("Pixel Difference: "+pixelDifference+", Tolerable Percentage Limit: "+displacedLineTolerancePercent);
        for(int i = 0; i<numberOfColumns;i++)
        {
            int xPos = 0;
            int xPos1 = 0;
            int test=0,test1=0,gotFirstLastLetterPosition = 0;
            int count = 0,count1 = 0;
            if(i == 0)
            {                                
                xPos = getLeftMostXPos(numberofRows);                
                for(int j = 0;j<numberofRows;j++)
                {
                    if(leftLetter[j][i]!= null && leftLetter[j][i].getCharacter()!= null)
                    {
                        indentation[j] = (int) leftLetter[j][i].getX()-xPos;
//                        System.out.println(indentation[j]);
                    }
                    else if(leftLetter[j][i] == null)
                    {
                        indentation[j] = 0;
                    }                    
                    //                    //<editor-fold defaultstate="collapsed" desc="comment">
    //                    System.out.println("Left test:"+test);
                        //                    else
                        //                    {
                        //                        test = 1;
                        //                        System.out.println("Left test:"+test);
                        //                    }
                        
//                    System.out.println("Right test:"+test1);
//                    else
//                    {
//                        test1 = 1;
//                        System.out.println("Right test:"+test1);
//                    }                    
//</editor-fold>                                            
                }
            }
            else
            {
                for(int j = 0;j<numberofRows;j++)
                {
                    if(j==0 && leftLetter[j][i]!= null && rightLetter[j][i]!= null && leftLetter[j][i].getCharacter()!=null && rightLetter[j][i].getCharacter()!=null && !" ".equals(leftLetter[j][i].getCharacter()) && !" ".equals(rightLetter[j][i].getCharacter()) && !"-".equals(leftLetter[j][i].getCharacter()) && !"-".equals(rightLetter[j][i].getCharacter()) )
                    {
                        xPos = (int)Math.ceil(leftLetter[j][i].getX());
                        xPos1 = (int)Math.ceil(rightLetter[j][i].getX());
//                        System.out.println("Header starts at:"+xPos+", and ends at"+xPos1);
                        gotFirstLastLetterPosition = 1;
                    }
                    else if(gotFirstLastLetterPosition == 0 && leftLetter[j][i]!= null && rightLetter[j][i]!= null && leftLetter[j][i].getCharacter()!=null && rightLetter[j][i].getCharacter()!=null&& !" ".equals(leftLetter[j][i].getCharacter()) && !" ".equals(rightLetter[j][i].getCharacter()) && !"-".equals(leftLetter[j][i].getCharacter()) && !"-".equals(rightLetter[j][i].getCharacter()) )
                    {
                        xPos = (int)Math.ceil(leftLetter[j][i].getX());
                        xPos1 = (int)Math.ceil(rightLetter[j][i].getX());
//                        System.out.println("Header starts at:"+xPos+", and ends at"+xPos1);
                        gotFirstLastLetterPosition = 1;
                    }
                    if(gotFirstLastLetterPosition == 1)
                    {
                        if(leftLetter[j][i]!= null && leftLetter[j][i].getCharacter()!= null)
                        {
//                            System.out.print("Columns starts at:"+(int)Math.ceil(leftLetter[j][i].getX()));
                            if(leftLetter[j][i]!= null && " ".equals(leftLetter[j][i].getCharacter()) || "-".equals(leftLetter[j][i].getCharacter()) )
                            {

                            }
                            else if(Math.abs(xPos - (int)Math.ceil(leftLetter[j][i].getX()))<=pixelDifference && test == 0)
                            {
                                test = 0;
                            }
                            else
                            {
                                xPos = (int)Math.ceil(leftLetter[j][i].getX());
//                                System.out.println("New Header Starts at:"+xPos);
                                test = 0;
                                count++;
                            }
                        }
                        else if(leftLetter[j][i] == null && test == 0)
                        {
                            test = 0;
                        }
                        else if(leftLetter[j][i] == null)
                        {
                        }
                        
                        if(rightLetter[j][i]!=null && rightLetter[j][i].getCharacter()!=null)
                        {
//                            System.out.println(" and ends at:"+(int)Math.ceil(rightLetter[j][i].getX()));
                            if(rightLetter[j][i]!=null && " ".equals(rightLetter[j][i].getCharacter()) || "-".equals(rightLetter[j][i].getCharacter()) )
                            {
                            }
                            else if(Math.abs(xPos1 - (int)Math.ceil(rightLetter[j][i].getX()))<=pixelDifference && test1 == 0)
                            {
                                test1 = 0;
                            }                        
                            else
                            {
                                xPos1 = (int)Math.ceil(rightLetter[j][i].getX());
//                                System.out.println("New Header ends at:"+xPos1);
                                test1 = 0;
                                count1++;
                            }
                        }
                        else if(rightLetter[j][i] == null && test1 == 0)
                        {
                            test1 = 0;                        
                        }
                        else if(rightLetter[j][i] == null)
                        { }
                    }
                }   
            }
//<editor-fold defaultstate="collapsed" desc="comment">
//            System.out.println("Test:"+count+"Test 1:"+count1+"percentageLimit:"+(count*100)/numberofRows+"anti percentageLimit:"+(numberofRows-count)*100/numberofRows);            
//            if(test == 0 && (count*100)/numberofRows<= displacedLineTolerancePercent)
//            {
//                allignmentCheck[i] = "left";
//                System.out.println("left allign");
//            }            
//            else 
//</editor-fold>
            if(i!=0)
            {
                if((count*100)/numberofRows<= displacedLineTolerancePercent)
                {
//                    System.out.println(("For Left Allignment"+(count*100)/numberofRows));
                    allignmentCheck[i] = "left";
//                    System.out.println("left allign");
                }
//<editor-fold defaultstate="collapsed" desc="comment">            
    //            else if(count*100/numberofRows>(99 - displacedLineTolerancePercent))
    //                {
    //                        allignmentCheck[i] = "left";
    //                        System.out.println("left allign");
    //                }
    //                else if(test1 == 0 && (count1*100)/numberofRows<= displacedLineTolerancePercent)
    //                {
    //                    allignmentCheck[i] = "right";
    //                    System.out.println("right allign");
    //                }
    //</editor-fold>
                else if((count1*100)/numberofRows<= displacedLineTolerancePercent)
                {
//                    System.out.println(("For Right Allignment"+(count1*100)/numberofRows));
                    allignmentCheck[i] = "right";
//                    System.out.println("right allign");
                }            
//<editor-fold defaultstate="collapsed" desc="comment">
    //            else if(count1*100/numberofRows>(99 - displacedLineTolerancePercent))
    //                {
    //                    allignmentCheck[i] = "right";
    //                    System.out.println("right allign");
    //                }
    //</editor-fold>
                else
                {
                    allignmentCheck[i] = "center";
//                    System.out.println("centre allign");
                }
            }
        }
        return indentation;
    }   
    
    public StringBuffer getTable(int numberofRows,int numberOfColumns) throws IOException{
        int[] indentetionLocal = findAllignment(numberofRows,numberOfColumns-1);
//        System.out.println(numberofRows+","+(numberOfColumns-1));
//        File fiile = new File("d:/table.html");
        StringBuffer sb = new StringBuffer();
        //BufferedWriter output = new BufferedWriter(new FileWriter(saveFile));
//        String title = saveFile.getName().substring(0, saveFile.getName().length()-5);
//        output.write("<html><head><title>"+title+"</title></head><body bgcolor="+"\"#008080\""+">");
        
        sb.append("<table width = 100%>");
        for(int i = 0; i<numberofRows;i++)
        {
            sb.append("<tr>");
            for(int j = 0;j<numberOfColumns-1;j++)
            {
                if(j==0)
                {                
                    if(tableData[i][j]!=null)                    
                    {
                        sb.append("<td style=\"padding-left:"+indentetionLocal[i]+"px;\">"+tableData[i][j]+"</td>");
//                        output.write(indentetionLocal[i]+"px;\">"+tableData[i][j]+"</td>");
//                    System.out.println("<td align = \""+allignmentCheck[j]+"\">"+tableData[i][j]+"</td>");
                    }
                    else
                    {
                        sb.append("<td>"+"</td>");
                    }
                    
                }
                else 
                {
                    if(tableData[i][j]!=null)                    
                    { 
                        sb.append("<td align = \""+allignmentCheck[j]+"\">"+tableData[i][j]+"</td>");
    //                    System.out.println("<td align = \""+allignmentCheck[j]+"\">"+tableData[i][j]+"</td>");
                    }
                    else
                    {
                        sb.append("<td>"+"</td>");
                    }
                }
//                if(j+1 == tableData[i].length)
//                {
//                    output.write("</tr>");
//                } 
            }
            sb.append("</tr>");
        }
        sb.append("</table>");
//        output.write(sb.toString()+"</body></html>");        
//        output.close();
        return sb;
    }
        
    public StringBuffer getTableWithAllCellsSpan(String pdfFile,int pageNumber,Rectangle[][] ColumnWiseRect,Rectangle wholeRectangle,int[][] cellSpan,
            int numberofRows,int numberOfColumns) throws IOException, CryptographyException{
        
//        Object[][] tablesData = new Object[numberofRows][numberOfColumns-1];
       for(int i =0;i<numberofRows;i++)
           for(int j =0;j<numberOfColumns;j++)
               ++cellSpan[i][j];
        
        int[] indentetionLocal = findAllignment(numberofRows,numberOfColumns-1);
        StringBuffer sb = new StringBuffer();
//        sb.append("<table style=\"border-collapse:collapse;\" border = \"1\" width = \"").append(wholeRectangle.width*2).append("px\">");
        sb.append("<table style=\"border-collapse:collapse;\" border = \"0\" width = \"100%\">");
        String tableHeader = "<tr heigth = \"2px\">";
        
        for(int i= 0;i<numberOfColumns-1;i++)
        {
            tableHeader = tableHeader.concat("<th width = \""+Math.ceil(ColumnWiseRect[0][i].width*100/wholeRectangle.width)+"%\"></th>");
        }
        
        tableHeader = tableHeader.concat("</tr>");
        sb.append(tableHeader);
        String columnAppend;
        String align;
        
        for(int i = 0; i<numberofRows;i++)
        {
            String tr = ColumnWiseRect[i][0].height*2 == 0 ? "<tr>": "<tr height =\"" + Math.ceil(ColumnWiseRect[i][0].width*100/wholeRectangle.width) + "%\">";
            sb.append(tr);
            for(int j = 0;j<numberOfColumns-1;j++)
            {
//                System.out.println("i :"+i+" j :"+j);
                int leftDifference = 0;
                int rightDifference = 0;
                if(j==0)
                {                
                    if(tableData[i][j]!=null && !" ".equals(tableData[i][j]) && getFirstSignificantChar(allCellsList[i][j],true)!=null )                    
                    {
                        if(cellSpan[ i ][ j ] > 1)
                        {
                            int width = 0;                            
                            for(int k =0;k<cellSpan[i][j];k++)
                                width += ColumnWiseRect[ i ][ j + k ].width;
                            
                            Rectangle r = new Rectangle(ColumnWiseRect[i][j].x, ColumnWiseRect[i][j].y, width, ColumnWiseRect[i][j].height);
                            tableData[ i ][ j ] = extractRegionText(r);                        
                        }
                        columnAppend = "<td colspan = \""+cellSpan[i][j]+"\" style=\"padding-left:"+indentetionLocal[i]+"px;\">"+tableData[i][j]+"</td>";
                        sb.append(columnAppend);
//                        tablesData[i][j] = columnAppend;
                        j= j + cellSpan[i][j]-1;
                    }
                    else
                    {
//                        String tempString = "<td></td>";
//                        sb.append(tempString); 
                        int noOfBlankColFound = 0;
                        for(int blankColSpan = j+1;blankColSpan<numberOfColumns-1;blankColSpan++)
                        {
                            if(cellSpan[i][blankColSpan]>1 || getFirstSignificantChar(allCellsList[i][blankColSpan], true)!=null)
                                break;
                            else if((i == 0)&& (numberCheck(tableData[i+1][blankColSpan])))
                                break;
                            else if((i>0&& i<numberofRows-1)&& (numberCheck(tableData[i-1][blankColSpan]) || numberCheck(tableData[i+1][blankColSpan])))
                                break;
                            else if((i == (numberofRows-1))&& (numberCheck(tableData[i-1][blankColSpan])))
                                break;
                            noOfBlankColFound++;
                        }
                        for(int in = 0;in<=noOfBlankColFound;in++)
                        {
                            cellSpan[i][j+in] = cellSpan[i][j+in] + noOfBlankColFound;
                        }
                        sb.append("<td colspan = \"").append(cellSpan[i][j]).append("\"></td>");
//                        tablesData[i][j] = "<td colspan = \"" + cellSpan[i][j] + "\"></td>";
                        j=j+noOfBlankColFound;
                    }                    
                }
                else 
                {
                    if(tableData[i][j]!=null && !" ".equals(tableData[i][j]) && getFirstSignificantChar(allCellsList[i][j],true)!=null )                    
                    { 
                        if(cellSpan[ i ][ j ] > 1)
                        {
                            int width = 0;
                            for(int k =0;k<cellSpan[i][j];k++)
                                width += ColumnWiseRect[ i ][ j + k ].width;
                            
                            Rectangle r = new Rectangle(ColumnWiseRect[i][j].x, ColumnWiseRect[i][j].y, width, ColumnWiseRect[i][j].height);
                            tableData[ i ][ j ] = extractRegionText(r);
                            if(getFirstSignificantChar(li,true)!=null)
                            {
                                float xx = getFirstSignificantChar(li,true).getX();
                                leftDifference = (int) (xx - r.x);
                                rightDifference = (int) ((r.x+r.width) - (getLastSignificantChar(li,true).getX()+getLastSignificantChar(li,true).getWidth()));
                            }                            
//<editor-fold defaultstate="collapsed" desc="comment">
                            //                            if(rightDifference<leftDifference && Math.abs(rightDifference-leftDifference)>5 && rightDifference<5)
                            //                                align = "right";
                            //                            else if(leftDifference<rightDifference && Math.abs(rightDifference-leftDifference)>5 && leftDifference<5)
                            //                                align = "left";
                            //</editor-fold>                            
                            if(rightDifference<5)
                                align = "right";
                            else if(leftDifference<5)
                                align = "left";
                            else 
                                align = "center";
                        }
                        else
                        {
                            if(leftLetter[i][j]!=null && !" ".equals(leftLetter[i][j].getCharacter()) && leftLetter[i][j].getCharacter()!=null && !"-".equals(leftLetter[i][j].getCharacter()))
                            {                                
                                leftDifference = (int) (leftLetter[i][j].getX() - rectangles[i][j].x);
                            }
                            if(rightLetter[i][j]!=null && !" ".equals(rightLetter[i][j].getCharacter()) && rightLetter[i][j].getCharacter()!=null && !"-".equals(rightLetter[i][j].getCharacter()))
                            {
                                rightDifference = (int) ((rectangles[i][j].x + rectangles[i][j].width) - (rightLetter[i][j].getX() + rightLetter[i][j].getWidth()));                                
                            }                            
//<editor-fold defaultstate="collapsed" desc="comment">
                            //                            if(rightDifference<leftDifference && Math.abs(rightDifference-leftDifference)>5 && rightDifference<5)
                            //                                align = "right";
                            //                            else if(leftDifference<rightDifference && Math.abs(rightDifference-leftDifference)>5 && leftDifference<5)
                            //                                align = "left";
                            //</editor-fold>
                            if(rightDifference<5)
                                align = "right";
                            else if(leftDifference<5)
                                align = "left";
                            else 
                                align = "center";
                        }
                        columnAppend = "<td colspan = \""+cellSpan[i][j]+"\" align = \""+align+"\">"+tableData[i][j]+"</td>";
                        sb.append(columnAppend);
//                        tablesData[i][j] = columnAppend;
                        j= j + cellSpan[i][j]-1;
                    }
                    else
                    {
//                        String tempString = "<td></td>";
//                        sb.append(tempString); 
                        int noOfBlankColFound = 0;
                        for(int blankColSpan = j+1;blankColSpan<numberOfColumns-1;blankColSpan++)
                        {
                            if(cellSpan[i][blankColSpan]>1 || getFirstSignificantChar(allCellsList[i][blankColSpan], true)!=null)
                                break;
                            else if((i == 0)&& (numberCheck(tableData[i+1][blankColSpan])))
                                break;
                            else if((i>0&& i<numberofRows-1)&& (numberCheck(tableData[i-1][blankColSpan]) || numberCheck(tableData[i+1][blankColSpan])))
                                break;
                            else if((i == (numberofRows-1))&& (numberCheck(tableData[i-1][blankColSpan])))
                                break;
                            noOfBlankColFound++;
                        }
                        for(int in = 0;in<=noOfBlankColFound;in++)
                        {
                            cellSpan[i][j+in] = cellSpan[i][j+in] + noOfBlankColFound;
                        }
                        sb.append("<td colspan = \"").append(cellSpan[i][j]).append("\"></td>");
//                        tablesData[i][j] = "<td colspan = \"" + cellSpan[i][j] + "\"></td>";
                        j=j+noOfBlankColFound;
                    }  
                }
            }
            sb.append("</tr>");
        }
        sb.append("</table>");
        if(document1 !=null)
            document1.close();
//        DrawTable dt = new DrawTable(table);
//        dt.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        dt.setSize(500,500);
//        dt.setVisible(true);
//        dt.setTitle("JTable");
        return sb;
    }
        
    public StringBuffer getListWithCellSpan(String pdfFile,int pageNumber,Rectangle[] ColumnWiseRect,Rectangle wholeRectangle,int[][] cellSpan,
            int numberofRows,int numberOfColumns) throws IOException, CryptographyException{
        
        StringBuffer sb = new StringBuffer();
        sb.append("<table style=\"border-collapse:collapse;\" border = \"0\" width = \"100%\">");        
        for(int i = 0; i<numberofRows;i++)
        {
            if(getFirstSignificantChar(allCellsList[i][0], true) != null){
                
                sb.append("<tr>");  
                tableData[i][0] = tableData[i][0].trim();
                String cellInHex = HexStringConverter.getHexStringConverterInstance().stringToHex(getFirstSignificantChar(allCellsList[i][0], true).getCharacter());
                isSymbol  = false;
                String singleCharacter = symbolCheck(cellInHex, getFirstSignificantChar(allCellsList[i][0], true));
                if(matchPattern(tableData[i][0]))
                {
                    sb.append("<td><p style=\"padding-left: ").append(getFirstSignificantChar(allCellsList[i][0], true).getX()).append("px\">").append(tableData[i][0].substring(start,end)).append("&nbsp&nbsp&nbsp").append(tableData[i][0].substring(end)).append("</p></td>");
                }
                else if(isSymbol)
                {
                    sb.append("<td><p style=\"padding-left: ").append(getFirstSignificantChar(allCellsList[i][0], true).getX()).append("px\">").append(singleCharacter).append("&nbsp&nbsp&nbsp").append(tableData[i][0].substring(foundCharAt+1)).append("</p></td>");
                }
                else 
                {                
                    sb.append("<td><p style=\"padding-left: ").append(getFirstSignificantChar(allCellsList[i][0], true).getX()).append("px\">").append(tableData[i][0]).append("</p></td>");
                }
                sb.append("</tr>");
            }
        }
        sb.append("</table>");
        return sb;
    }
    
    private boolean positionCheck(float currentPosition)
    {
        Collections.sort(listOfDifferentPositionOfTD);
        for(int i = 0;i<listOfDifferentPositionOfTD.size();i++)
        {
            if(Math.abs(currentPosition - listOfDifferentPositionOfTD.get(i)) > 3)
            {
                return true;
            }
        }
        return false;
    }
      
    int layerNumber = 0;
    List<Float> listOfDifferentPositionOfTD = new ArrayList();
    public StringBuffer getListWithAllCellSpan(String pdfFile,int pageNumber,Rectangle[] ColumnWiseRect,Rectangle wholeRectangle,int[][] cellSpan,
            int numberofRows,int numberOfColumns) throws IOException, CryptographyException{
        
        StringBuffer sb = new StringBuffer();
        sb.append("<table style=\"border-collapse:collapse;\" border = \"0\" width = \"100%\">"); 
        for(int i = 0; i<numberofRows;i++)
        {
            if(getFirstSignificantChar(allCellsList[i][0], true) != null)
            {                
//                sb.append("<tr>");  
                tableData[i][0] = tableData[i][0].trim();
                String cellInHex = HexStringConverter.getHexStringConverterInstance().stringToHex(getFirstSignificantChar(allCellsList[i][0], true).getCharacter());
                String singleCharacter = symbolCheck(cellInHex, getFirstSignificantChar(allCellsList[i][0], true));
                if(i == 0)
                {
                    if(matchPattern(tableData[i][0]))
                    {
                        if(positionCheck(getFirstSignificantChar(allCellsList[i][0], true).getX()))
                            listOfDifferentPositionOfTD.add(getFirstSignificantChar(allCellsList[i][0], true).getX());
                        sb.append("<tr><td valign =\"top\"><p style=\"padding-left: ").append(getFirstSignificantChar(allCellsList[i][0], true).getX()).append("px\">").append(tableData[i][0].substring(start,end)).append("</td><td>").append(tableData[i][0].substring(end));
                    }
                    else if(isSymbol)
                    {
                        if(positionCheck(getFirstSignificantChar(allCellsList[i][0], true).getX()))
                            listOfDifferentPositionOfTD.add(getFirstSignificantChar(allCellsList[i][0], true).getX());
                        sb.append("<tr><td valign =\"top\"><p style=\"padding-left: ").append(getFirstSignificantChar(allCellsList[i][0], true).getX()).append("px\">").append(singleCharacter).append("</td><td>").append(tableData[i][0].substring(foundCharAt+1));                        
                    }
                    else 
                        sb.append(tableData[i][0]);
                }
                else
                {
                    if(matchPattern(tableData[i][0]))
                    {
                        if(positionCheck(getFirstSignificantChar(allCellsList[i][0], true).getX()))
                            listOfDifferentPositionOfTD.add(getFirstSignificantChar(allCellsList[i][0], true).getX());
                        sb.append("</p></td></tr><tr><td valign =\"top\"><p style=\"padding-left: ").append(getFirstSignificantChar(allCellsList[i][0], true).getX()).append("px\">").append(tableData[i][0].substring(start,end)).append("</td><td>").append(tableData[i][0].substring(end));
                    }
                    else if(isSymbol)
                    {
                        if(positionCheck(getFirstSignificantChar(allCellsList[i][0], true).getX()))
                            listOfDifferentPositionOfTD.add(getFirstSignificantChar(allCellsList[i][0], true).getX());
                        sb.append("</p></td></tr><tr><td valign =\"top\"><p style=\"padding-left: ").append(getFirstSignificantChar(allCellsList[i][0], true).getX()).append("px\">").append(singleCharacter).append("</td><td>").append(tableData[i][0].substring(foundCharAt+1));
                    } 
                    else
                        sb.append(tableData[i][0]);
                }
//                if(matchPattern(tableData[i][0]))
//                {
//                    sb.append("<td><p style=\"padding-left: ").append(getFirstSignificantChar(allCellsList[i][0], true).getX()).append("px\">").append(tableData[i][0].substring(start,end)).append("&nbsp&nbsp&nbsp").append(tableData[i][0].substring(end)).append("</p></td>");
//                }
//                else if(isSymbol)
//                {
//                    sb.append("<td> <p style=\"padding-left: ").append(getFirstSignificantChar(allCellsList[i][0], true).getX()).append("px\">").append(singleCharacter).append("&nbsp&nbsp&nbsp").append(tableData[i][0].substring(foundCharAt+1)).append("</p></td>");
//                }
//                else 
//                {                
//                    sb.append(tableData[i][0]);
//                }
//                sb.append("</tr>");
            }
        }
        sb.append("</p></td></tr></table>");
        return sb;
    }
    
    
    private boolean matchPattern(String texts){
        
        matcher = pattern.matcher(texts);
        try{
            matcher = matcher.region(0, 10);
//            System.out.println("Matches: "+matcher.find());
        }
        catch(Exception ex)
        {
            return false;
        }
        if(matcher.find())
        {
            start = matcher.start();
            end = matcher.end();
            return true;
        }
//        System.out.println("Matches Starts at"+matcher.start()+"; and Ends at"+matcher.end());
        return false;
    }
        
    public String symbolCheck(String cellInHex, TextPosition text){
        if("e280a2".equals(cellInHex))
        {
            isSymbol = true;
            return "&bull;&nbsp&nbsp&nbsp&nbsp"; 
        }
        else if("ef82b7".equals(cellInHex))
        {
            isSymbol = true;
            return "&bull;&nbsp&nbsp&nbsp&nbsp";            
        }
        else if("ef82a7".equals(cellInHex))
        {
            isSymbol = true;
            return "&#9632;&nbsp&nbsp&nbsp&nbsp";            
        }
        else if("ef82ae".equals(cellInHex))
        {
            isSymbol = true;
            return "&rarr;&nbsp&nbsp&nbsp&nbsp";            
        }
        else if("ef83bc".equals(cellInHex))
        {
            isSymbol = true;
            return "&#8730;&nbsp&nbsp&nbsp&nbsp";           
        }
        else if("ef8398".equals(cellInHex))
        {
            isSymbol = true;
            return "&#f0d8;&nbsp&nbsp&nbsp&nbsp";            
        }
        else if("ef81b6".equals(cellInHex))
        {
            isSymbol = true;
            return "&#f076;&nbsp&nbsp&nbsp&nbsp";          
        }
        else
        {
            isSymbol = false;
            return text.getCharacter().replace("<", "&lt;").replace(">", "&gt;");            
        }
    }
    
    public void getList(int numberofRows,int numberOfColumns, File saveFile) throws IOException{
//        int[] indentetionLocal = findAllignment(numberofRows,numberOfColumns-1);
//        System.out.println(numberofRows+","+(numberOfColumns-1));
//        File fiile = new File("d:/table.html");
        BufferedWriter output = new BufferedWriter(new FileWriter(saveFile));
        String title = saveFile.getName().substring(0, saveFile.getName().length()-5);
        output.write("<html><head><title>"+title+"</title></head><body bgcolor="+"\"#008080\""+"><table border="+25+"% width = "+60+"% align = center bgcolor = pink>");
        for(int i = 0; i<numberofRows;i++)
        {
            for(int j = 0;j<numberOfColumns-1;j++)
            {                
                if(tableData[i][j]!=null)                    
                {                     
                    String cellInHex = HexStringConverter.getHexStringConverterInstance().stringToHex(tableData[i][j]);
//                    System.out.println(tableData[i][j]+"--------->"+cellInHex);
                    if(cellInHex.equals("e280a220") && i == 0 && j == 0)
                    {
                        output.write("<tr><td valign=top>&#8729;</td><td>");
                    }
                    else if(cellInHex.equals("e280a220"))
                    {
                        output.write("</td></tr><tr><td valign=top>&#8729;</td><td>");
                    }
                    else
                        output.write(tableData[i][j]);
                }
            }
        }
        output.write("</td></tr></table></body></html>");
        output.close();
    }
        
    public boolean numberCheck(String cell){
        Pattern pat = Pattern.compile(numberPattern);
        Matcher match = pat.matcher(cell);
        boolean bol = match.find();
//        System.out.println("Cell :"+cell+", isNumber? = "+bol);
        return bol;         
    }
        
    public void getTable(int numberofRows,int numberOfColumns,boolean bol) throws IOException{
        int[] indentetionLocal = findAllignment(numberofRows,numberOfColumns-1);
//        System.out.println(numberofRows+","+(numberOfColumns-1));
//        File fiile = new File("d:/table.html");
        BufferedWriter output = new BufferedWriter(new FileWriter("D://a.out"));
        //String title = saveFile.getName().substring(0, saveFile.getName().length()-5);
        output.write("<html><head><title>"+"What"+"</title></head><body bgcolor="+"\"#008080\""+"><table border="+25+"% width = "+60+"% align = center bgcolor = pink>");
        for(int i = 0; i<numberofRows;i++)
        {
            output.write("<tr>");
            for(int j = 0;j<numberOfColumns-1;j++)
            {
                if(j==0)
                {                
                    if(tableData[i][j]!=null)                    
                    { 
                        output.write("<td style=\"padding-left:");
                        output.write(indentetionLocal[i]+"px;\">"+tableData[i][j]+"</td>");
//                    System.out.println("<td align = \""+allignmentCheck[j]+"\">"+tableData[i][j]+"</td>");
                    }
                    else
                        output.write("<td>"+"</td>");
                }
                else 
                {
                    if(tableData[i][j]!=null)                    
                    { 
                        output.write("<td align = \""+allignmentCheck[j]+"\">"+tableData[i][j]+"</td>");
    //                    System.out.println("<td align = \""+allignmentCheck[j]+"\">"+tableData[i][j]+"</td>");
                    }
                    else
                        output.write("<td>"+"</td>");
                }
//                if(j+1 == tableData[i].length)
//                {
//                    output.write("</tr>");
//                } 
            }
            output.write("</tr>");
        }
        output.write("</table></body></html>");
        output.close();
    }
    
    public List[][] getAllCellsWithTextProperties(){
        return allCellsList;
    }
       
    public void ExtractTextByArea(Rectangle[][] rect,int numberOfRows,int numberOfColumns)throws IOException, CryptographyException{
        
        rectangles = rect;
        try
        {           
            for(int row = 0; row<numberOfRows;row++)
            {
                for(int column = 0; column < numberOfColumns ; column++)
                {
                    String className = "class"+row+"-"+column;
                    stripper.addRegion( className, rect[row][column] );
                }
            }
            
            stripper.extractRegions( firstPage );
            
            for(int row = 0; row<numberOfRows;row++)
            {
                for(int column = 0; column < numberOfColumns ; column++)
                {        
                    String className = "class"+row+"-"+column;
                    List TextinArea1 = stripper.regionCharacterList.get(className);
                    List<TextPosition> lis = (List<TextPosition>) TextinArea1.get(0);
                    allCellsList[row][column] = lis;
//                    tableData[row][column] = stripper.getTextForRegion( className );
                    getTextWithBoldItalicProp(lis);
                    tableData[row][column] = replaceAllWeiredChars(tempForParagraph).toString();
                    leftLetter[row][column] = getFirstSignificantChar(lis,false);
                    rightLetter[row][column] = getLastSignificantChar(lis,false);
                }
            }
        }
        finally
        {
            if( document != null )
            {
                document.close();
            }
        }        
    }   
        
    public void ExtractTextByArea1(String filename, Rectangle[] rect,int pageNumber,int numberOfRows,int numberOfColumns) 
            throws IOException, CryptographyException{
                
        PDF.pdmodel.PDDocument document = null;
        try
        {
            document = PDDocument.load( filename );
            if( document.isEncrypted() )
            {
                try
                {
                    document.decrypt( "" );
                }
                catch( InvalidPasswordException e )
                {
                    System.err.println( "Error: Document is encrypted with a password." );
                    System.exit( 1 );
                }
            }
            PDFTextStripperByArea strip = new PDFTextStripperByArea(1);
            strip.setSortByPosition( true );
            List allPages = document.getDocumentCatalog().getAllPages();
            PDPage firstPage = (PDPage)allPages.get( pageNumber );
            // annotations
            List ann = firstPage.getAnnotations();
            
            
            for(int row = 0; row<numberOfRows;row++)
            {
                for(int column = 0; column < numberOfColumns ; column++)
                {
//                    System.out.println("Row :"+row+", Column :"+column);
                    strip.addRegion( "class1", rect[row] );
                    strip.extractRegions( firstPage );            
                    List TextinArea1 = strip.regionCharacterList.get("class1");
                    List<TextPosition> li = (List<TextPosition>) TextinArea1.get(0);
                    allCellsList[row][column] = li;
//                    getTextWithBoldItalicProp(li);
                    tableData[row][column] = strip.getTextForRegion( "class1" );//stripper.getColumntext();
//                    tableData[row][column] = replaceAllWeiredChars(tempForParagraph).toString();
                    leftLetter[row][column] = getFirstSignificantChar(li,false);
                    rightLetter[row][column] = getLastSignificantChar(li,false);
//                    leftLetter[row][column] = stripper.getLeftLetter();
//                    rightLetter[row][column] = stripper.getRightLetter();
                }
            }
            
//                System.out.println("Index:"+rowNumber+","+columnNumber+".Left letter:"+leftLetter[rowNumber][columnNumber].getCharacter()+", Right Letter:"+rightLetter[rowNumber][columnNumber].getCharacter());
//                System.out.println( "Text in the area:" + rect );
//                System.out.println( stripper.getTextForRegion( "class1" ) );
        }
        finally
        {
            if( document != null )
            {
                document.close();
            }
        }        
    }   
        
    public static void main( String[] args ) throws Exception{
        if( args.length != 1 )
        {
            usage();
        }
        else
        {
            PDDocument document = null;
            try
            {
                document = PDDocument.load( args[0] );
                if( document.isEncrypted() )
                {
                    try
                    {
                        document.decrypt( "" );
                    }
                    catch( InvalidPasswordException e )
                    {
                        System.err.println( "Error: Document is encrypted with a password." );
                        System.exit( 1 );
                    }
                }
                PDFTextStripperByArea stripper = new PDFTextStripperByArea();
                stripper.setSortByPosition( true );
                Rectangle rect = new Rectangle( 10, 280, 275, 60 );
                stripper.addRegion( "class1", rect );
                List allPages = document.getDocumentCatalog().getAllPages();
                PDPage firstPage = (PDPage)allPages.get( 0 );
                stripper.extractRegions( firstPage );
//                System.out.println( stripper.getTextForRegion( "class1" ) );

            }
            finally
            {
                if( document != null )
                {
                    document.close();
                }
            }
        }
    }

    public String extractRegionText(Rectangle rect) 
            throws IOException, CryptographyException{
            
        stripper1.addRegion( "class1", rect );
        stripper1.extractRegions( firstPage1 );
        Vector TextinArea1 = stripper1.regionCharacterList.get("class1");
        li = (List<TextPosition>) TextinArea1.get(0);
        getTextWithBoldItalicProp(li);
        String region = replaceAllWeiredChars(tempForParagraph).toString();
        return region;       
    }
    
    private void getTextWithBoldItalicProp(List<TextPosition> lis)
    {
        PDBorderStyleDictionary pd = new PDBorderStyleDictionary();
        tempForParagraph = new StringBuffer();
        TextPosition t = getFirstSignificantChar(lis, false);
        String fontString = "";
        
        if(t!=null)
        {
            PDFont font = t.getFont();
            PDFontDescriptor fontDescriptor = font.getFontDescriptor();

            if (fontDescriptor != null) {
                    fontString = fontDescriptor.getFontName();    			
            }
            else {
                    fontString = "";	
            }            
            tempForParagraph.append("<p style=\"font-size: ").append((int)(t.getFontSizeInPt()*2)).append("px; font-family:").append(fontString).append(";\">");
        }
        
        lastIsBold = false;
        lastIsItalic = false;
        TextPosition text;
        for(int i = 0; i<lis.size();i++)
        {
            text = lis.get(i);
            processTextt(text);
        }
        if(lastIsBold)
        {
            tempForParagraph.append("</b>");
            lastIsBold = false;
        }
        if(lastIsItalic)
        {
            tempForParagraph.append("</i>");
            lastIsItalic = false;
        }
        if(t!=null)
            tempForParagraph.append("</p>");
    }
    
    public void processTextt( TextPosition text )
    {
    	try 
        {
            int marginLeft = (int)((text.getXDirAdj())*1);
            int fontSizePx = Math.round(text.getFontSizeInPt()/72*72*1);
            int marginTop = (int)((text.getYDirAdj())*1-fontSizePx);

            String fontString = "";
            PDFont font = text.getFont();
            PDFontDescriptor fontDescriptor = font.getFontDescriptor();

            if (fontDescriptor != null) {
                    fontString = fontDescriptor.getFontName();    			
            }
            else {
                    fontString = "";	
            }

            int indexPlus = fontString.indexOf("+");
            if (indexPlus != -1) {
                    fontString = fontString.substring(indexPlus+1);
            }
            boolean isBold = fontString.contains("Bold");
            boolean isItalic = fontString.contains("Italic");

            int indexDash = fontString.indexOf("-");
            if (indexDash != -1) {
                    fontString = fontString.substring(0, indexDash);
            }
            int indexComa = fontString.indexOf(",");
            if (indexComa != -1) {
                    fontString = fontString.substring(0, indexComa);
            }
            assignBoldItalicForTableData(text, marginLeft,  marginTop, fontSizePx, fontString, isBold, isItalic);
        } 
        catch (IOException e) 
        {	
        }    
    }	
    
    StringBuffer tempForParagraph = new StringBuffer();
    
    private void assignBoldItalicForTableData(TextPosition text, int marginLeft, int marginTop, int fontSizePx, String fontString, boolean isBold, boolean isItalic) throws IOException 
    {
        
//        System.out.println("Character "+text.getCharacter()+", ASCII value :"+(int)text.getCharacter().toCharArray()[0]+" Font Size "+text.getFontSizeInPt()+", Font-Family :"+fontString+", Bold: "+isBold+", Italic :"+isItalic);
        if (isBold && !lastIsBold ) 
        {
            tempForParagraph.append("<b>");
            lastIsBold = true;
        }
        if (isItalic && !lastIsItalic)
        {
            tempForParagraph.append("<i>");
            lastIsItalic = true;
        }
        if (!isItalic && lastIsItalic)
        {
            tempForParagraph.append("</i>");
            lastIsItalic = false;
        }         
        if (lastIsBold && !isBold)
        {
            tempForParagraph.append("</b>");
            lastIsBold = false;
        }
        appendCharacter(text);
        
    }
        
    public void appendCharacter(TextPosition text)
    {
        int charInDecimal = (int)text.getCharacter().toCharArray()[0];
        if(charInDecimal>255)
        {
            tempForParagraph.append("&#"+(int)text.getCharacter().toCharArray()[0]+";");
        }
        else
        {
            if(charInDecimal == 0)
            {
                
            }
//            else if(charInDecimal == 32)
//                tempForParagraph.append("&#32;");
            else if(charInDecimal == 38)
                tempForParagraph.append("&#38;");
            else
                tempForParagraph.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
        }
    }
    
    private StringBuffer replaceAllWeiredChars(StringBuffer sb){
        String ss = sb.toString();
        ss = ss.replace("•", "&bull;").replace("®", "&#174;").replace("†", "&#8224;").replace("’", "&#8217;").replace("”", "&#8221;").replace("“", "&#8220;")
                .replace("—", "&#8212;").replace("–", "&#8211;").replace(" ", " ").replace("©", "&#169;").replace("­","&#8211;");
        StringBuffer stringBuffer = new StringBuffer(ss);
        return stringBuffer;
    }
    
    public TextPosition getLastSignificantChar(List<TextPosition> cellText, boolean merged){
    
        if(merged)
        {
            for (int rowPos = cellText.size() - 1; rowPos >=0 ; rowPos--) {
                if(!cellText.get(rowPos).getCharacter().equals(" ")){
                    return cellText.get(rowPos);
                }
            }
        }
        else
        {
            for (int rowPos = cellText.size() - 1; rowPos >=0 ; rowPos--) {
                if(!cellText.get(rowPos).getCharacter().equals(" ") && !cellText.get(rowPos).getCharacter().equals(")")){
                    return cellText.get(rowPos);
                }
            }
        }
            return null;
        }
        
    public TextPosition getFirstSignificantChar(List<TextPosition> cellText, boolean merged){
    
        if(merged)
        {
            for (int rowPos = 0; rowPos < cellText.size() ; rowPos++) {
                if(!cellText.get(rowPos).getCharacter().equals(" ")){
                    foundCharAt = rowPos;
                    return cellText.get(rowPos);
                }
            }
        }
        else
        {
            for (int rowPos = 0; rowPos < cellText.size() ; rowPos++) {
                if(!cellText.get(rowPos).getCharacter().equals(" ") && !cellText.get(rowPos).getCharacter().equals("(")){
                    return cellText.get(rowPos);
                }
            }
        }
        
        return null;
    }
    
   private static void usage(){
        System.err.println( "Usage: java org.apache.pdfbox.examples.util.ExtractTextByColumn <input-pdf>" );
    }

}