package util;

import java.util.HashMap;

/**
 * 
 * @author Akram
 * 
 */
public class SpecialCharacterToUnicode
{
	private HashMap<Integer, Integer> wingdings = new HashMap<Integer, Integer>();
	private HashMap<Integer, Integer> wingdings2 = new HashMap<Integer, Integer>();
	private HashMap<Integer, Integer> symbol = new HashMap<Integer, Integer>();
	private HashMap<Integer, Integer> webdings = new HashMap<Integer, Integer>();
	
	private HashMap<String, Integer> specialChar = new HashMap<String, Integer>();
	
	public SpecialCharacterToUnicode(){		
		setUnicodeForWingdings();
		setUnicodeForWingdings2();
		setUnicodeForSymbol();
		setUnicodeForWebdings();
		setSpecialChar();
	}
	
	
	public int getUnicodeCode(String fontCode)
	{
		return specialChar.get(fontCode);
	}
	public int getUnicodeCode(int fontCode, int fontSerial)
	{
		
		switch (fontSerial)
		{
			case 1:
				return wingdings.get(fontCode);		
			case 2:
				return wingdings2.get(fontCode);
			case 3:
				return symbol.get(fontCode);
			case 4:
				return webdings.get(fontCode);
			
			default:
				return 0;
		}
		
	}
	public String[] supportSpecialChar()
	{
		String[] supportedSpecialChar= new String[] {"–","—","“","”","™","‘","’","©","®"};
		
		return supportedSpecialChar;
	}
	public boolean supportedFonts(int fontCode, int fontSerial){
		boolean supported = false;
		
		int[] supportedWingdingsCode =  new int[]  {108,110, 111, 112, 113, 114,115,116,117,118, 120, 167, 168,216, 251, 252, 253, 254};
		
		int[] supportedWingdings2Code =  new int[]  {42, 48, 79, 80, 81, 82, 83, 84, 151,152,160,161,162,163,184};
		
		int[] supportedSymbolCode =  new int[]  {176,183};
		
		int[] supportedWebdingsCode =  new int[]  {97, 99, 127};
		
		switch (fontSerial)
		{
			case 1:
				for(int i=0; i<supportedWingdingsCode.length; i++){
					if(supportedWingdingsCode[i] == fontCode){
						supported = true;
					}
				}				
				break;
			case 2:
				for(int i=0; i<supportedWingdings2Code.length; i++){
					if(supportedWingdings2Code[i] == fontCode){
						supported = true;
					}
				}				
				break;
			case 3:
				for(int i=0; i<supportedSymbolCode.length; i++){
					if(supportedSymbolCode[i] == fontCode){
						supported = true;
					}
				}			
				break;
			case 4:
				for(int i=0; i<supportedWebdingsCode.length; i++){
					if(supportedWebdingsCode[i] == fontCode){
						supported = true;
					}
				}			
				break;
			default:
				break;
		}
		return supported;
	}
	private void setSpecialChar()
	{
		specialChar.put("–", 8211);
		specialChar.put("—", 8212);
		specialChar.put("“", 8220);
		specialChar.put("”", 8221);
		specialChar.put("™", 8482);
		specialChar.put("‘", 8216);
		specialChar.put("’", 8217);
		specialChar.put("©", 169);
		specialChar.put("®", 174);
		
	
		
		
	}
	private void setUnicodeForWingdings(){
		wingdings.put(108, 9679);
		wingdings.put(110, 9632);
		wingdings.put(111, 9471);
		wingdings.put(112, 9723);
		wingdings.put(113, 10065);
		wingdings.put(114, 10066);
		wingdings.put(115, 9670);
		wingdings.put(116, 9670);
		wingdings.put(117, 9670);
		wingdings.put(118, 10070);
		wingdings.put(120, 9746);
		wingdings.put(167, 9632);
		wingdings.put(168, 9723);
		wingdings.put(216, 10146);
		wingdings.put(251, 10007);
		wingdings.put(252, 10003);
		wingdings.put(253, 9746);
		wingdings.put(254, 9745);
	}
	
	private void setUnicodeForSymbol(){		
		symbol.put(183, 9679);		
		symbol.put(176, 176);
	}

	private void setUnicodeForWingdings2(){
		wingdings2.put(42, 9723);
		wingdings2.put(48, 9723);
		wingdings2.put(79, 10007);
		wingdings2.put(80, 10003);
		wingdings2.put(81, 9746);
		wingdings2.put(82, 9745);
		wingdings2.put(83, 9746);
		wingdings2.put(84, 9746);
		wingdings2.put(151, 9679);
		wingdings2.put(152, 9679);
		wingdings2.put(160, 9632);
		wingdings2.put(161, 9632);
		wingdings2.put(162, 9632);
		wingdings2.put(163, 9723);
		wingdings2.put(184, 9674);
	}
	
	private void setUnicodeForWebdings(){
		webdings.put(97, 10003);
		webdings.put(99, 9723);
		webdings.put(127, 9647);
	}	

}
