/*
 * To change this template, choose Tools | Templates
 * and open the template imageNumber the editor.
 */
package pdfreader;

/**
 *
 * @author Zia
 */

import PDF.exceptions.CryptographyException;
import PDF.pdmodel.PDDocument;
import PDF.pdmodel.PDPage;
import PDF.pdmodel.font.PDFont;
import PDF.pdmodel.font.PDFontDescriptor;
import PDF.util.ImageIOUtil;
import PDF.util.PDFTextStripper;
import PDF.util.PDFTextStripperByArea;
import PDF.util.TextPosition;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HtmlFileGenerate extends PDFTextStripper
{
    private BufferedWriter htmlFile;
    private int type = 0;
    private float zoom = (float) 2; 
    private int lastMarginTop = 0;
    private int max_gap = 15;
    float previousAveCharWidth = -1;
    private int resolution = 72; //default resolution
    private boolean needToStartNewSpan = false;
    private int lastMarginLeft = 0;
    private int lastMarginRight = 0;
    private int numberSpace = 0;
    private int sizeAllSpace = 0;
    private boolean addSpace;
    private int startXLine;
    private boolean wasBold = false;
    private boolean wasItalic = false;
    private int lastFontSizePx = 0;
    private String lastFontString = "";
    List<PDPage> pages = null;
    private StringBuffer currentLine = new StringBuffer();
    List<TableFromSpan> tableFromSpan = new ArrayList();
    TableFromSpan tfs;
    int spanCounter = 0;
    String pdfPath;   
    ListExtraction listt;
    List<TextPosition> TextinAr;
    int imageNum = 0;

   /**
    * Public constructor
    * @param outputFileName The html file
    * @param type represents how we are going to create the html file
    * 			0: we create a new block for every letters
    * 			1: we create a new block for every words
    * 			2: we create a new block for every line 
    * 			3: we create a new block for every line - using a cache to set the word-spacing property
    * @param zoom 1.5 - 2 is a good range
    * @throws IOException
    */
    
    public HtmlFileGenerate(String pathOfPdfFile,String outputFileName, int type, float zoom) throws IOException{
//        pdfPagePanel = pdfPagePane;
        pdfPath = pathOfPdfFile;
    	try 
        {
            htmlFile = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFileName),"UTF8"));
            String header = "<html><head><title>"+outputFileName+"</title></head><body>";
            htmlFile.write(header);
            this.type = type;
            this.zoom= zoom;		
        }
    	catch (UnsupportedEncodingException e) 
        {
            System.err.println( "Error: Unsupported encoding." );
            System.exit( 1 );		
        }
    	catch (FileNotFoundException e) 
        {
            System.err.println( "Error: File not found." );
            System.exit( 1 );		
        }
    	catch (IOException e) 
        {
            System.err.println( "Error: IO error, could not open html file." );
            System.exit( 1 );	
        }	
    }
   
    /**
     * Close the HTML file
     */
    
    public void closeFile() {		
            try 
            {
		htmlFile.close();	
            } 
            catch (IOException e) 
            {
                e.printStackTrace();            
                System.err.println( "Error: IO error, could not close html file." );            
                System.exit( 1 );		
            }
	}
		
    /**
     * Convert a PDF file to HTML
     *
     * @param pathToPdf Path to the PDF file
     *
     * @throws IOException If there is an error processing the operation.
     */
        
    Vector getRegionsText(TaggedRegion t, List<PDPage> allPages) throws IOException{
        PDFTextStripperByArea stripper = new PDFTextStripperByArea(1);
        stripper.setSortByPosition( true );
        stripper.addRegion( "class1", t.rectangle );
        PDPage firstPage = (PDPage)allPages.get( t.pageNumber );
        stripper.extractRegions( firstPage );
        String region = stripper.getTextForRegion( "class1" );
        return stripper.regionCharacterList.get("class1");             
    }
        
           
    ExtractTextByArea extractTextByArea;
    public StringBuffer convertPdfToHtml(List<TaggedRegion> tR,List<PDPage> allPages) throws Exception{	
        
        this.pages = allPages;        
        ExtractTextByArea ETB;
        List<TextPosition> TextinArea;
        ListExtraction list;
        int imageNumber = 0;
        StringBuffer allRegionsAsString = new StringBuffer();
        int currentPageNumber = 0;
        for(int i =0;i<tR.size();i++)
        {
            System.out.print("Region "+i+": ");
            StringBuffer strBuffer;
            TaggedRegion t = tR.get(i);
            if(currentPageNumber == 0)
            {
                allRegionsAsString.append("<Pages>\n\t<Page no = \"").append(t.pageNumber+1).append("\">\n");
                currentPageNumber = t.pageNumber;
            }
            else if(currentPageNumber!=t.pageNumber)
            {
                currentPageNumber = t.pageNumber;
                allRegionsAsString.append("\t</Page>\n\t<Page no = \"").append(t.pageNumber+1).append("\">");
            }
            allRegionsAsString.append("\t\t<Region x = \"").append(t.rectangle.x).append("\" y = \"").append(t.rectangle.y).append("\" width = \"").append(t.rectangle.width).append("\" height = \"").append(t.rectangle.height).append("\" type = \"").append(t.tag).append("\">\n\t\t<HtmlContent>\n\t\t\t");
            
            if("table".equals(t.tag))
            {
//                System.out.println("is a Table.");
//                htmlFile.write(getTableSinglePixel(pdfPath,t.rectangle,t.pageNumber).toString());
                allRegionsAsString.append(getTableSinglePixel(t.rectangle,t.pageNumber).toString());                
            }
            else if("paragraph".equals(t.tag))
            {
//                System.out.println("is a Paragraph.");
                ETB = new ExtractTextByArea();
                TextinArea = ETB.extractRegionTextAllOnce(pdfPath, t.rectangle, t.pageNumber, 0);
                list = new ListExtraction(5,2);
                for (Iterator<TextPosition> it = TextinArea.iterator(); it.hasNext();) 
                {
                    TextPosition text = it.next();
//                    System.out.println("Chars :"+text.getCharacter());
                    list.processTextt(text);
                }
                strBuffer = replaceAllWeiredChars(list.getParagraph());
                allRegionsAsString.append(strBuffer.toString());
//                htmlFile.write(strBuffer.toString());
            }
            else if("text_with_line_break".equals(t.tag))
            {
//                System.out.println("is a text_with_line_break.");
                ETB = new ExtractTextByArea();
                TextinArea = ETB.extractRegionTextAllOnce(pdfPath, t.rectangle, t.pageNumber, 0);
                list = new ListExtraction(3,2);
                for (Iterator<TextPosition> it = TextinArea.iterator(); it.hasNext();) 
                {
                    TextPosition text = it.next();
                    list.processTextt(text);
                }
                strBuffer = replaceAllWeiredChars(list.getParagraph());
                allRegionsAsString.append(strBuffer.toString());
//                htmlFile.write(strBuffer.toString());
            }
            else if("list".equals(t.tag))
            {
//                System.out.println("is a List.");
                allRegionsAsString.append(getListSinglePixel(pdfPath,t.rectangle,t.pageNumber).toString());
//                htmlFile.write(getListSinglePixel(pdfPath,t.rectangle,t.pageNumber).toString()); 
            }
            else if("image".equals(t.tag))
            {
//                System.out.println("is an Image.");
                allRegionsAsString.append(saveImage(pdfPath,t.pageNumber,t.rectangle,imageNumber).toString());                
                imageNumber++;
            }
            
            allRegionsAsString.append("\n\t\t</HtmlContent>\n\t\t</Region>\n");
        }
        allRegionsAsString.append("\t</Page>\n</Pages>");
        endHtml();
        return allRegionsAsString;
    }
    public StringBuffer getHtmlContent(int pageNumber, Rectangle rec, String type)
    {
        StringBuffer htmlContent = null;
        if("table".equals(type))
        {
            try {
                htmlContent = getTableSinglePixel(rec,pageNumber);
            } catch (CryptographyException ex) {
                Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if("paragraph".equals(type))
        {
            try {
                extractTextByArea = new ExtractTextByArea();
                TextinAr = extractTextByArea.extractRegionTextAllOnce(pdfPath, rec, pageNumber, 0);
                listt = new ListExtraction(5,2);
                for (Iterator<TextPosition> it = TextinAr.iterator(); it.hasNext();) 
                {
                    TextPosition text = it.next();
                    listt.processTextt(text);
                }
                htmlContent = replaceAllWeiredChars(listt.getParagraph());
            } catch (IOException ex) {
                Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
            } catch (CryptographyException ex) {
                Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if("text_with_line_break".equals(type))
        {
            try {
                extractTextByArea = new ExtractTextByArea();
                TextinAr = extractTextByArea.extractRegionTextAllOnce(pdfPath, rec, pageNumber, 0);
                listt = new ListExtraction(3,2);
                for (Iterator<TextPosition> it = TextinAr.iterator(); it.hasNext();) 
                {
                    TextPosition text = it.next();
                    listt.processTextt(text);
                }
                htmlContent = replaceAllWeiredChars(listt.getParagraph());
            } catch (IOException ex) {
                Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
            } catch (CryptographyException ex) {
                Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if("list".equals(type))
        {
            try {
                htmlContent = getListSinglePixel(pdfPath,rec,pageNumber);
            } catch (CryptographyException ex) {
                Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if("image".equals(type))
        {
            htmlContent = saveImage(pdfPath,pageNumber,rec,imageNum);                
            imageNum++;
        }
        return htmlContent;
    }

    private StringBuffer saveImage(String path,int pNumber,Rectangle rect,int imageNumber){
        try 
        {
            PDDocument doc = PDDocument.load(path);
            pages = doc.getDocumentCatalog().getAllPages();
            PDPage pageToSave = (PDPage)pages.get(pNumber);
            BufferedImage pageAsImage = pageToSave.convertToImage();
            pageAsImage = pageAsImage.getSubimage(rect.x*2, rect.y*2, rect.width*2, rect.height*2);
            String imageFilename = path;
            if (imageFilename.toLowerCase().endsWith(".pdf"))
            {
                imageFilename = imageFilename.substring(0, imageFilename.length()-4);
            }
            imageFilename += "-" + (pNumber + imageNumber); 
            ImageIOUtil.writeImage(pageAsImage, "jpeg", imageFilename,  BufferedImage.TYPE_USHORT_565_RGB, 300);
            String ss = "<p padding-left = "+rect.x+"px><img src=\""+imageFilename+".jpg\" "+ "height="+rect.height*2+" width="+rect.width*2+"></p>";            
            return new StringBuffer(ss);
        }
        catch (IOException exception)
        {
            exception.printStackTrace();
        }
        return null;
    }

    void endHtml(){
        try {
            htmlFile.write("</body></html>");
            
            closeFile();
        } catch (IOException ex) {
            Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public StringBuffer getTableSinglePixel(Rectangle rectangle,int currentPage) throws CryptographyException{           
        
        int dividedRegionWidth = 1;
        StringBuffer sb = null;
        int[] regioon = null;
        ExtractTextByAreaSinglePixel ETB = new ExtractTextByAreaSinglePixel();
        try {
            regioon = ETB.extractTextByArea(pdfPath,rectangle,currentPage,0);
        } catch (IOException ex) {
            Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        long before = System.currentTimeMillis();
        int positionOfRowStart[] = ETB.getPointOfRowStart();
        int numberofRows = ETB.returnNumberofRows();
        int numberofColumns = ETB.returnNumberofColumns();
        
        List<TextPosition>[][] aCWTP;
       
        ExtractTextByColumn ETBC = new ExtractTextByColumn(pdfPath,currentPage); 
        
        Rectangle[][] ColumnWiseRect = new Rectangle[numberofRows][numberofColumns-1];
        int[][] cellSpan = new int[numberofRows][numberofColumns];     
        
        for(int row = 0; row < numberofRows ; row++)
        {
            for(int column = 0; column < numberofColumns - 1 ; column++)
            {
                if(column ==0 && row==0)
                    ColumnWiseRect[row][column] = new Rectangle(rectangle.x,rectangle.y,(regioon[column+1]*dividedRegionWidth-rectangle.x),positionOfRowStart[row]-rectangle.y);

                else if(row == 0 && column>0)
                    ColumnWiseRect[row][column] = new Rectangle(regioon[column]*dividedRegionWidth,rectangle.y,(regioon[column+1]*dividedRegionWidth-regioon[column]*dividedRegionWidth),positionOfRowStart[row]-rectangle.y);

                else if(column == 0 && row>0)
                    ColumnWiseRect[row][column] = new Rectangle(rectangle.x,positionOfRowStart[row-1],(regioon[column+1]*dividedRegionWidth-rectangle.x),(positionOfRowStart[row]-positionOfRowStart[row-1]));

                else if(column>0 && row>0)
                    ColumnWiseRect[row][column] = new Rectangle(regioon[column]*dividedRegionWidth,positionOfRowStart[row-1],(regioon[column+1]*dividedRegionWidth-regioon[column]*dividedRegionWidth),(positionOfRowStart[row]-positionOfRowStart[row-1]));
            }
        }
        try {
            ETBC.ExtractTextByArea(ColumnWiseRect,numberofRows,numberofColumns-1);
        } catch (IOException ex) {
            Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        aCWTP = ETBC.getAllCellsWithTextProperties();
        for(int rowPos=0;rowPos<numberofRows;rowPos++)
        {
            for(int columnPos = 1;columnPos<numberofColumns-1;columnPos++)
            {
                if(aCWTP[rowPos][columnPos] != null  && aCWTP[rowPos][columnPos].size() >= 1 )
                { 
                    TextPosition firstColLastChar = getLastSignificantChar(aCWTP,rowPos,columnPos-1);
                    TextPosition secColFirstChar = getFirstSignificantChar(aCWTP,rowPos,columnPos);
                    float defaultWordSpace = 5;
                    if((firstColLastChar != null && secColFirstChar != null) )
                    {
                        float gap = secColFirstChar.getX() - (firstColLastChar.getX() + firstColLastChar.getWidth());
                        if(spanCounter>0)
                            defaultWordSpace = 5f;
                        if(gap<=defaultWordSpace)
                        {                        
                            int currentSpan = cellSpan [ rowPos ] [ columnPos  - spanCounter - 1 ];
                            int executeStartPos = columnPos - currentSpan - spanCounter - 1 ;
                            for(int k = executeStartPos; k <= executeStartPos + currentSpan + spanCounter + 1; k ++)
                            {
                                cellSpan[ rowPos ] [ k ] = currentSpan + spanCounter + 1;
                            }
                        }
                    }
                    spanCounter = 0;
                }
                
            }
        }
        
//        pdfPagePanel.pdfpagepanelrec = ColumnWiseRect;
//        pdfPagePanel.repaint();
//        long after = System.currentTimeMillis();  
//        System.out.println("Execution Time to set the text to their cell: " + (after - before) + "ms"); 
        try {
            sb = ETBC.getTableWithAllCellsSpan(pdfPath,currentPage,ColumnWiseRect,rectangle,cellSpan,numberofRows,numberofColumns);
        } catch (IOException ex) {
            Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
        }
        sb = replaceAllWeiredChars(sb);
        return sb;
    }
    
    private StringBuffer replaceAllWeiredChars(StringBuffer sb){
        String ss = sb.toString();
        ss = ss.replace("•", "&bull;").replace("®", "&#174;").replace("†", "&#8224;").replace("’", "&#8217;").replace("”", "&#8221;").replace("“", "&#8220;")
                .replace("—", "&#8212;").replace("–", "&#8211;").replace(" ", " ").replace("©", "&#169;").replace("­","&#8211;");
        StringBuffer stringBuffer = new StringBuffer(ss);
        return stringBuffer;
    }
        
    public StringBuffer getListSinglePixel(String pdfFile,Rectangle rectangle,int currentPage) throws CryptographyException{           
        
        StringBuffer sb = null;
        ExtractTextByAreaSinglePixel ETB = new ExtractTextByAreaSinglePixel();
        try {
            ETB.extractTextByArea(pdfFile,rectangle,currentPage,0);
        } catch (IOException ex) {
            Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
        }
        long before = System.currentTimeMillis();
        int positionOfRowStart[] = ETB.getPointOfRowStart();
        int numberofRows = ETB.returnNumberofRows();
        int numberofColumns = ETB.returnNumberofColumns();

        ExtractTextByColumn ETBC = new ExtractTextByColumn();
        Rectangle[] ColumnWiseRect = new Rectangle[numberofRows];
        int[][] cellSpan = new int[numberofRows][numberofColumns];        
        
        for(int row = 0; row < numberofRows ; row++)
        {
            if(row==0)
                ColumnWiseRect[row] = new Rectangle(rectangle.x,rectangle.y,rectangle.width,positionOfRowStart[row]-rectangle.y);
            else if(row>0)
                ColumnWiseRect[row] = new Rectangle(rectangle.x,positionOfRowStart[row-1],rectangle.width,(positionOfRowStart[row]-positionOfRowStart[row-1]));
        }
        
        try {
            ETBC.ExtractTextByArea1(pdfFile,ColumnWiseRect,currentPage,numberofRows,2-1);
        } catch (IOException ex) {
            Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //        //<editor-fold defaultstate="collapsed" desc="comment">
//        aCWTP = ETBC.getAllCellsWithTextProperties();
        //        for(int rowPos=0;rowPos<numberofRows;rowPos++)
        //        {
        //            for(int columnPos = 1;columnPos<numberofColumns-1;columnPos++)
        //            {
        //                if(aCWTP[rowPos][columnPos] != null  && aCWTP[rowPos][columnPos].size() >= 1 )
        //                {
        //                    //TextPosition firstColLastChar = aCWTP[rowPos][columnPos-1].get((aCWTP[rowPos][columnPos - 1].size()-1));
        //                    //TextPosition secColFirstChar = aCWTP[rowPos][columnPos].get(0);
        //                    TextPosition firstColLastChar = getLastSignificantChar(aCWTP,rowPos,columnPos-1);
        //                    TextPosition secColFirstChar = getFirstSignificantChar(aCWTP,rowPos,columnPos);
        ////                    System.out.println("i :"+rowPos+", j :"+columnPos+", Span Counter :"+spanCounter);
        //                    float defaultWordSpace = 5;
        //                    if((firstColLastChar != null && secColFirstChar != null) )
        //                    {
        ////                        System.out.println("For GAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP");
        //                        float gap = secColFirstChar.getX() - (firstColLastChar.getX() + firstColLastChar.getWidth());
        //                        if(spanCounter>0)
        //                            defaultWordSpace = 5f;
        //                        if(gap<=defaultWordSpace)
        //                        {
        //                            int currentSpan = cellSpan [ rowPos ] [ columnPos  - spanCounter - 1 ];
        ////                            System.out.println("Current Span :"+currentSpan+" at the position: "+rowPos+" ,"+(columnPos - spanCounter - 1));
        //                            int executeStartPos = columnPos - currentSpan - spanCounter - 1 ;
        //                            for(int k = executeStartPos; k <= executeStartPos + currentSpan + spanCounter + 1; k ++)
        //                            {
        //                                cellSpan[ rowPos ] [ k ] = currentSpan + spanCounter + 1;
        ////                                System.out.println("Cell Span :"+cellSpan[rowPos][k]+" at "+rowPos+", "+k);
        //                            }
        //                        }
        //                    }
        ////                    if(spanCounter > 0 && flagForSpanCounter)
        ////                    {
        ////                        System.out.println("For Span Counterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr");
        ////                            int currentSpan = cellSpan [ rowPos ] [ columnPos - spanCounter -1];
        ////                            System.out.println("Current Span :"+currentSpan+" at the position: "+rowPos+" ,"+(columnPos - spanCounter -1));
        ////                            int executeStartPos = columnPos - currentSpan - spanCounter ;
        ////                            for(int k = executeStartPos; k <= executeStartPos + currentSpan + spanCounter + 1; k ++)
        ////                            {
        ////                                cellSpan[ rowPos ] [ k ] = currentSpan +spanCounter+ 1;
        ////                                System.out.println("Cell Span :"+cellSpan[rowPos][k]+" at "+rowPos+", "+k);
        ////                            }
        ////                            flagForSpanCounter = false;
        ////                    }
        //                    spanCounter = 0;
        //                }
        //            }
        //        }
        //</editor-fold>
        
        long after = System.currentTimeMillis();  
//        System.out.println("Execution Time to set the text to their cell: " + (after - before) + "ms"); 
        try {
            sb = ETBC.getListWithCellSpan(pdfFile,currentPage,ColumnWiseRect,rectangle,cellSpan,numberofRows,numberofColumns);
        } catch (IOException ex) {
            Logger.getLogger(HtmlFileGenerate.class.getName()).log(Level.SEVERE, null, ex);
        }
        sb = replaceAllWeiredChars(sb);
        return sb;
    }
       
    public TextPosition getLastSignificantChar(List<TextPosition>[][] cellText, int rowPos, int columnPos){
        for (int i = columnPos; i >= 0; i--) 
        {
            if(cellText[rowPos][i].size()>0)
            {
                for (int j = cellText[rowPos][i].size() - 1; j >=0 ; j--) 
                {
                    if(!cellText[rowPos][i].get(j).getCharacter().equals(" "))
                    {
                        return cellText[rowPos][i].get(j);
                    }                
                }
            }
            spanCounter++;
        }
        return null;
    }
    
    public TextPosition getFirstSignificantChar(List<TextPosition>[][] cellText,int row,int col){
        
        for (int i = 0; i < cellText[row][col].size() ; i++) {
            if(!cellText[row][col].get(i).getCharacter().equals(" ") && !cellText[row][col].get(i).getCharacter().equals(")")){
                return cellText[row][col].get(i);
            }
        }
        return null;
    }
    
	
    /**
     * A method provided as an event interface to allow a subclass to perform
     * some specific functionality when text needs to be processed.
     *
     * @param text The text to be processed
     */
    @Override
    protected void processTextPosition( TextPosition text ){
    	try 
        {
            int marginLeft = (int)((text.getXDirAdj())*zoom);
            int fontSizePx = Math.round(text.getFontSizeInPt()/72*resolution*zoom);
            int marginTop = (int)((text.getYDirAdj())*zoom-fontSizePx);


            String fontString = "";
            PDFont font = text.getFont();
            PDFontDescriptor fontDescriptor = font.getFontDescriptor();

            if (fontDescriptor != null) {
                    fontString = fontDescriptor.getFontName();    			
            }
            else {
                    fontString = "";	
            }

            int indexPlus = fontString.indexOf("+");
            if (indexPlus != -1) {
                    fontString = fontString.substring(indexPlus+1);
            }
            boolean isBold = fontString.contains("Bold");
            boolean isItalic = fontString.contains("Italic");

            int indexDash = fontString.indexOf("-");
            if (indexDash != -1) {
                    fontString = fontString.substring(0, indexDash);
            }
            int indexComa = fontString.indexOf(",");
            if (indexComa != -1) {
                    fontString = fontString.substring(0, indexComa);
            }
            switch (type)
            {                    
                case 0:
                renderingSimple(text, marginLeft,  marginTop, fontSizePx, fontString, isBold, isItalic);
                break;
                case 1:
                        renderingGroupByWord(text, marginLeft,  marginTop, fontSizePx, fontString, isBold, isItalic);
                break;
                case 2:
                        renderingGroupByLineNoCache(text, marginLeft,  marginTop, fontSizePx, fontString, isBold, isItalic);
                break;
                case 3:
                        renderingGroupByLineWithCache(text, marginLeft,  marginTop, fontSizePx, fontString, isBold, isItalic);
                break;
                default:
                renderingSimple(text, marginLeft,  marginTop, fontSizePx, fontString, isBold, isItalic);
                break;
            }
        } 
        catch (IOException e) 
        {	
            e.printStackTrace();
        }    
    }	
        
    /** 
     * The method that given one character is going to write it imageNumber the HTML file.
     * 
     * @param text
     * @param marginLeft
     * @param marginTop
     * @param fontSizePx
     * @param fontString
     * @param isBold
     * @param isItalic
     * @throws IOException 

     */    
    private void renderingSimple(TextPosition text, int marginLeft, int marginTop, int fontSizePx, String fontString, boolean isBold, boolean isItalic)
            throws IOException {            
        htmlFile.write("<span style=\"position: absolute; margin-left:"+marginLeft+"px; margin-top: "+marginTop+"px; font-size: "+fontSizePx+"px; font-family:"+fontString+";");
        if (isBold) {
                htmlFile.write("font-weight: bold;");
        }
        if (isItalic) {
                htmlFile.write("font-style: italic;");
        }
        htmlFile.write("\">");

        htmlFile.write(text.getCharacter());
        System.out.println(text.getCharacter());

        htmlFile.write("</span>"); 
    }
        
    /** 
     * The method that given one character is going to write it only if it's the end of a word imageNumber the HTML file.
     * 
     * @param text
     * @param marginLeft
     * @param marginTop
     * @param fontSizePx
     * @param fontString
     * @param isBold
     * @param isItalic
     * @throws IOException 

     */    
    private void renderingGroupByWord(TextPosition text, int marginLeft, int marginTop, int fontSizePx, String fontString, boolean isBold, boolean isItalic) 
            throws IOException {    
        if (lastMarginTop == marginTop) 
        {   	
            if ((needToStartNewSpan) || (wasBold != isBold) || (wasItalic != isItalic) || (lastFontSizePx != fontSizePx) || (lastMarginLeft>marginLeft) || (marginLeft-lastMarginRight>max_gap))
            {    	
                if (lastMarginTop != 0) 
                {    		
                    htmlFile.write("</span>");    			
                }
                htmlFile.write("<span style=\"position: absolute; margin-left:"+marginLeft+"px; margin-top: "+marginTop+"px; font-size: "+fontSizePx+"px; font-family:"+fontString+";");
                if (isBold) 
                {
                    htmlFile.write("font-weight: bold;");
                }
                if (isItalic) 
                {
                    htmlFile.write("font-style: italic;");
                }
                htmlFile.write("\">");
                needToStartNewSpan = false;   			
            }   			
            if (text.getCharacter().equals(" "))
            {
                htmlFile.write(" ");  			    			
                needToStartNewSpan = true;
            }
            else 
            {
                htmlFile.write(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
            }	        
        }		
        else 
        {	
            if (text.getCharacter().equals(" ")) 
            {
                htmlFile.write("&nbsp;");
                needToStartNewSpan = true;		
            }	
            else 
            {    	
                needToStartNewSpan = false;      
                if (lastMarginTop != 0)                 
                {
                    htmlFile.write("</span>");    		
                }
                htmlFile.write("<span style=\"position: absolute; margin-left:"+marginLeft+"px; margin-top: "+marginTop+"px; font-size: "+fontSizePx+"px; font-family:"+fontString+";");        	
                if (isBold) 
                {        			
                    htmlFile.write("font-weight: bold;");        		
                }
                if (isItalic) 
                {                        
                    htmlFile.write("font-style: italic;");
                }
                htmlFile.write("\">");
                htmlFile.write(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));  
            }	
            lastMarginTop = marginTop;		
        }   		
        lastMarginLeft = marginLeft;
        lastMarginRight = (int) (marginLeft + text.getWidth());
        wasBold = isBold;
        wasItalic = isItalic;
        lastFontSizePx = fontSizePx;	
    }
   
    /** 
     * The method that given one character is going to write it only if it's the end of a line imageNumber the HTML file.
     * 
     * @param text
     * @param marginLeft
     * @param marginTop
     * @param fontSizePx
     * @param fontString
     * @param isBold
     * @param isItalic
     * @throws IOException 

     */
    private void renderingGroupByLineNoCache(TextPosition text, int marginLeft, int marginTop, int fontSizePx, String fontString, 
            boolean isBold, boolean isItalic) throws IOException {		
        if (lastMarginTop == marginTop) 
        {   			
            if (lastMarginLeft>marginLeft) 
            {    			
                htmlFile.write("</span>");
            	htmlFile.write("<span style=\"position: absolute; margin-left:"+marginLeft+"px; margin-top: "+marginTop+"px; font-size: "+fontSizePx+"px; font-family:"+fontString+";");
                if (isBold) 
                {                       
                    htmlFile.write("font-weight: bold;");
                }
                if (isItalic) 
                {                       
                    htmlFile.write("font-style: italic;");
                }
                htmlFile.write("\">");
       		
            }
            lastMarginTop = marginTop;
        }
        else 
        {
            if (lastMarginTop != 0) 
            {                
                htmlFile.write("</span>");
            }	
            htmlFile.write("<span style=\"position: absolute; margin-left:"+marginLeft+"px; margin-top: "+marginTop+"px; font-size: "+fontSizePx+"px; font-family:"+fontString+";");
            if (isBold) 
            {
                    htmlFile.write("font-weight: bold;");
            }
            if (isItalic) 
            {
                    htmlFile.write("font-style: italic;");
            }			
            htmlFile.write("\">");			
            lastMarginTop = marginTop;		
        }
        htmlFile.write(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
        lastMarginLeft = marginLeft;	
    }	

    /** 
     * The method that given one character is going to write it only if it's the end of a line imageNumber the HTML file.
     * A cache is used to set the word-spacing property.
     * 
     * @param text
     * @param marginLeft
     * @param marginTop
     * @param fontSizePx
     * @param fontString
     * @param isBold
     * @param isItalic
     * @throws IOException 

     */
    
    private void renderingGroupByLineWithCache(TextPosition text, int marginLeft, int marginTop, int fontSizePx, String fontString, 
            boolean isBold, boolean isItalic) throws IOException {    	
        if (marginLeft-lastMarginRight> text.getWidthOfSpace()) 
        {    		
            currentLine.append(" "); 
            sizeAllSpace += (marginLeft-lastMarginRight);
            numberSpace++;
            addSpace = false;
    	}
    	if ((lastMarginTop != marginTop) || (!lastFontString.equals(fontString)) || (wasBold != isBold) || (wasItalic != isItalic) || (lastFontSizePx != fontSizePx) || (lastMarginLeft>marginLeft) || (marginLeft-lastMarginRight>150))
        {
            if (lastMarginTop != 0) 
            {				
                boolean display = true;				
                // if the bloc is empty, we do not display it (for a lighter result)
                if (currentLine.length() == 1) 
                {					
                    char firstChar = currentLine.charAt(0);
                    if (firstChar == ' ') 
                    {
                            display = false;
                    }
                }
                if (display) 
                {
                    tfs = new TableFromSpan();
                    if (numberSpace != 0) 
                    {
                        int spaceWidth = Math.round(((float) sizeAllSpace)/((float) numberSpace)-text.getWidthOfSpace());    
                        htmlFile.write("<span style=\"position: absolute; margin-left:"+startXLine+"px; margin-top: "+lastMarginTop+"px; font-size: "+lastFontSizePx+"px; font-family:"+lastFontString+";");
                    }
                    else 
                    {                		
                        htmlFile.write("<span style=\"position: absolute; margin-left:"+startXLine+"px; margin-top: "+lastMarginTop+"px; font-size: "+lastFontSizePx+"px; font-family:"+lastFontString+";");
                    }
                    if (wasBold) 
                    {            			
                        htmlFile.write("font-weight: bold;");   
                        tfs.isBold = true;
                    }            		
                    if (wasItalic)                         
                    {            			
                        htmlFile.write("font-style: italic;");
                        tfs.isItalic = true;
                    }        			
                    htmlFile.write("\">");
                    tfs.startXLine = startXLine;
                    tfs.lastMarginTop = lastMarginTop;
                    tfs.lastFontSizePx = lastFontSizePx;
                    tfs.lastFontString = lastFontString;
                    tfs.spannedText = currentLine.toString();
                    tableFromSpan.add(tfs);
                    htmlFile.write(currentLine.toString());
                    System.out.println(currentLine.toString());
                    htmlFile.write("</span>\n");
                }			
            }   			
            numberSpace = 0;   			
            sizeAllSpace = 0;
            currentLine = new StringBuffer();
            startXLine = marginLeft;
            lastMarginTop = marginTop;
            wasBold = isBold;
            wasItalic = isItalic;
            lastFontSizePx = fontSizePx;
            lastFontString = fontString;
            addSpace = false;		
        }		
        else 
        {
            int sizeCurrentSpace = (int) (marginLeft-lastMarginRight-text.getWidthOfSpace());
            if (sizeCurrentSpace > 5) 
            {
                if (lastMarginTop != 0) 
                {
                    tfs = new TableFromSpan();
                    if (numberSpace != 0) 
                    {
                        int spaceWidth = Math.round(((float) sizeAllSpace)/((float) numberSpace)-text.getWidthOfSpace());                		
                        htmlFile.write("<span style=\"position: absolute; margin-left:"+startXLine+"px; margin-top: "+lastMarginTop+"px; font-size: "+lastFontSizePx+"px; font-family:"+lastFontString+";");        			
                    }
                    else 
                    {
                        htmlFile.write("<span style=\"position: absolute; margin-left:"+startXLine+"px; margin-top: "+lastMarginTop+"px; font-size: "+lastFontSizePx+"px; font-family:"+lastFontString+";");        			
                    }
                    if (wasBold) 
                    {
                        htmlFile.write("font-weight: bold;");
                        tfs.isBold = true;
                    }
            		
                    if (wasItalic) 
                    {                            
                        htmlFile.write("font-style: italic;");
                        tfs.isItalic = true;
                    }
                    htmlFile.write("\">");
                    tfs.startXLine = startXLine;
                    tfs.lastMarginTop = lastMarginTop;
                    tfs.lastFontSizePx = lastFontSizePx;
                    tfs.lastFontString = lastFontString;
                    tfs.spannedText = currentLine.toString();
                    tableFromSpan.add(tfs);
                    htmlFile.write(currentLine.toString());
                    System.out.println(currentLine.toString());
                    htmlFile.write("</span>\n");    			
                }       			
                numberSpace = 0;
                sizeAllSpace = 0;
                currentLine = new StringBuffer();
                startXLine = marginLeft;
                lastMarginTop = marginTop;
                wasBold = isBold;
                wasItalic = isItalic;
                lastFontSizePx = fontSizePx;
                lastFontString = fontString;
                addSpace = false;    		
            }
            else 
            {
                if (addSpace) 
                {
                    currentLine.append(" ");
                    sizeAllSpace += (marginLeft-lastMarginRight);
                    numberSpace++;
                    addSpace = false;   	   			
                }	
            }
        }
        if (text.getCharacter().equals(" ")) 
        {
            addSpace = true;
            //sizeAllSpace += text.getWidthOfSpace();
        }
        else 
        {
            currentLine.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));   		
        }			
        lastMarginLeft = marginLeft;
        lastMarginRight = (int) (marginLeft + text.getWidth()*zoom);	
    }

    private void endTable() {
        tfs = new TableFromSpan();
        tfs.startXLine = startXLine;
        tfs.lastMarginTop = lastMarginTop;
        tfs.lastFontSizePx = lastFontSizePx;
        tfs.lastFontString = lastFontString;
        tfs.isBold = wasBold;
        tfs.isItalic = wasItalic;
        tfs.spannedText = currentLine.toString();
        tableFromSpan.add(tfs);        
    }

    private void getAllCell() {
        int topChange = tableFromSpan.get(0).lastMarginTop;
        for(int i=0; i<tableFromSpan.size();i++)
        {
            if(Math.abs(topChange-tableFromSpan.get(i).lastMarginTop)>3)
            {
                System.out.println();
                topChange = tableFromSpan.get(i).lastMarginTop;
            }
            System.out.print("Span "+i+": "+tableFromSpan.get(i).spannedText+"  ");
        }
    }
}